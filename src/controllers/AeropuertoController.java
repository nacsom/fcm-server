package controllers;

import database.ConectorDB;
import view.VentanaAeropuerto;
import view.VentanaInformacionAeropuerto;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by Anton
 */


public class AeropuertoController implements ActionListener {

    private VentanaAeropuerto aeropuertoView;
    private VentanaInformacionAeropuerto ventanaInformacionAeropuerto;

    public AeropuertoController(VentanaAeropuerto aeropuertoView, VentanaInformacionAeropuerto ventanaInformacionAeropuerto){
        this.aeropuertoView = aeropuertoView;
        this.ventanaInformacionAeropuerto = ventanaInformacionAeropuerto;
    }

    public void actionPerformed(ActionEvent event) {
        if (event.getActionCommand().equals("Aceptar")) {

            String referencia = aeropuertoView.getJtfAeropuertoId().getText();
            int coordx = (int) aeropuertoView.getJtfCoordenadaX().getValue();
            String nombre = aeropuertoView.getJtfAeropuertoNombre().getText();
            int coordy = (int) aeropuertoView.getJtfCoordenadaY().getValue();

            if (referencia.equals("") || referencia.length() >= 50) {
                if (referencia.equals("")) {
                    JOptionPane.showMessageDialog(aeropuertoView,
                            "La referencia no puede estar vacia.",
                            "Input error",
                            JOptionPane.ERROR_MESSAGE);
                } else {
                    JOptionPane.showMessageDialog(aeropuertoView,
                            "La referencia no puede contener mas de 50 caracteres.",
                            "Input error",
                            JOptionPane.ERROR_MESSAGE);
                }
            } else if (nombre.equals("") || nombre.length() >= 50) {
                if (nombre.equals("")) {
                    JOptionPane.showMessageDialog(aeropuertoView,
                            "El nombre no puede estar vacío.",
                            "Input error",
                            JOptionPane.ERROR_MESSAGE);
                } else {
                    JOptionPane.showMessageDialog(aeropuertoView,
                            "El nombre no puede contener mas de 50 caracteres.",
                            "Input error",
                            JOptionPane.ERROR_MESSAGE);
                }
            } else {

                ConectorDB conn = ConectorDB.getInstance();
                if(conn.existeDbAeropuerto(referencia)){
                    JOptionPane.showMessageDialog(aeropuertoView,
                            "El aeropuerto ya existe.",
                            "Input error",
                            JOptionPane.ERROR_MESSAGE);
                }else{
                    conn.newDbAeropuerto(referencia, nombre, coordx, coordy);
                    ventanaInformacionAeropuerto.actualizaTablaAeropuertos();
                }

            }


        } else if (event.getActionCommand().equals("Eliminar")) {

            String referencia = aeropuertoView.getJtfAeropuertoId().getText();

            if (referencia.equals("") || referencia.length() >= 50) {
                if (referencia.equals("")) {
                    JOptionPane.showMessageDialog(aeropuertoView,
                            "La referencia no puede estar vacia.",
                            "Input error",
                            JOptionPane.ERROR_MESSAGE);
                } else {
                    JOptionPane.showMessageDialog(aeropuertoView,
                            "La referencia no puede contener mas de 50 caracteres.",
                            "Input error",
                            JOptionPane.ERROR_MESSAGE);
                }
            } else {

                //ELIMINAR AEROPUERTO
                ConectorDB conn = ConectorDB.getInstance();
                if (conn.existeDbAeropuerto(referencia)) {
                    conn.deleteDbAeropuerto(referencia);
                    ventanaInformacionAeropuerto.actualizaTablaAeropuertos();
                    JOptionPane.showMessageDialog(aeropuertoView,
                            "El Aeropuerto deseado ha sido destruido.",
                            "Input error",
                            JOptionPane.OK_OPTION);
                }else{
                    JOptionPane.showMessageDialog(aeropuertoView,
                            "La referencia no existe.",
                            "Input error",
                            JOptionPane.ERROR_MESSAGE);
                }
            }
        }
    }
}
