package controllers;

import view.ViewServidor;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
/**
		* Created by Gabriella
		*/
public class MenuController implements ActionListener {

	// VISTA
	private ViewServidor view;


	public MenuController(ViewServidor view) {
		this.view = view;

	}

	public void actionPerformed(ActionEvent event) {



		if (event.getActionCommand().equals("Vuelo")) {
			view.mostraVentanaVuelo();
		}


		if (event.getActionCommand().equals("Avion")) {
			view.mostraVentanaAvion();
		}


		if (event.getActionCommand().equals("Aeropuerto")) {
			view.mostraVentanaAeropuerto();

		}


		if (event.getActionCommand().equals("Vuelos")) {
			view.mostraVentanaInformacionVuelo();

		}


		if (event.getActionCommand().equals("Aviones")) {
			view.mostraVentanaInformacionAvion();
		}


		if (event.getActionCommand().equals("Aeropuertos")) {
			view.mostraVentanaInformacionAeropuerto();
		}



	}

}








