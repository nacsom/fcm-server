package controllers;

import database.ConectorDB;
import view.VentanaAvion;
import view.VentanaInformacionAvion;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by Anton
 */

public class AvionController implements ActionListener {

    private VentanaAvion avionView;
    private VentanaInformacionAvion ventanaInformacionAvion;

    public AvionController(VentanaAvion avionView, VentanaInformacionAvion ventanaInformacionAvion){
        this.ventanaInformacionAvion = ventanaInformacionAvion;
        this.avionView = avionView;
    }

    public void actionPerformed(ActionEvent event) {
        if(event.getActionCommand().equals("Aceptar")){

            String referencia = avionView.getJtfAvion().getText();
            int altura = (int) avionView.getJtfAltura().getValue();
            String modelo = avionView.getJtfModeloA().getText();
            int plazas = (int) avionView.getJtfPlazas().getValue();

            if (referencia.equals("") || referencia.length() >= 50 ){
                if(referencia.equals("")){
                    JOptionPane.showMessageDialog(avionView,
                            "La referencia no puede estar vacia.",
                            "Input error",
                            JOptionPane.ERROR_MESSAGE);
                }else {
                    JOptionPane.showMessageDialog(avionView,
                            "La referencia no puede contener mas de 50 caracteres.",
                            "Input error",
                            JOptionPane.ERROR_MESSAGE);
                }
            }else   if (modelo.equals("") || modelo.length() >= 50 ) {
                if (modelo.equals("")) {
                    JOptionPane.showMessageDialog(avionView,
                            "El modelo no puede estar vacio.",
                            "Input error",
                            JOptionPane.ERROR_MESSAGE);
                } else {
                    JOptionPane.showMessageDialog(avionView,
                            "El modelo no puede contener mas de 50 caracteres.",
                            "Input error",
                            JOptionPane.ERROR_MESSAGE);
                }
            }else{

                ConectorDB conn = ConectorDB.getInstance();
                if(conn.existeDbAvion(referencia)){
                    JOptionPane.showMessageDialog(avionView,
                            "El avion ya existe.",
                            "Input error",
                            JOptionPane.ERROR_MESSAGE);
                }else{
                    conn.newDbAvion(referencia, modelo, plazas, altura);
                    ventanaInformacionAvion.actualizaTablaAviones();
                }
            }


        }else if(event.getActionCommand().equals("Eliminar")){

            String referencia = avionView.getJtfAvion().getText();

            if (referencia.equals("") || referencia.length() >= 50 ){
                if(referencia.equals("")){
                    JOptionPane.showMessageDialog(avionView,
                            "La referencia no puede estar vacia.",
                            "Input error",
                            JOptionPane.ERROR_MESSAGE);
                }else {
                    JOptionPane.showMessageDialog(avionView,
                            "La referencia no puede contener mas de 50 caracteres.",
                            "Input error",
                            JOptionPane.ERROR_MESSAGE);
                }
            }else{
                //ELIMINAR AVION
                ConectorDB conn = ConectorDB.getInstance();
                if(conn.existeDbAvion(referencia)){
                    conn.deleteDbAvion(referencia);
                    ventanaInformacionAvion.actualizaTablaAviones();
                    JOptionPane.showMessageDialog(avionView,
                            "El Avion deseado se ha decomisionado correctamente.",
                            "Input error",
                            JOptionPane.OK_OPTION);
                }else{
                    JOptionPane.showMessageDialog(avionView,
                            "La referencia no existe.",
                            "Input error",
                            JOptionPane.ERROR_MESSAGE);
                }
            }

        }
    }

}
