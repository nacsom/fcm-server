package controllers;

import database.ConectorDB;
import view.DateTimePicker;
import view.VentanaInformacionVuelo;
import view.VentanaVuelo;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Date;

/**
 * Created by Anton
 */

public class VueloController implements ActionListener {

    private VentanaVuelo vueloView;
    private VentanaInformacionVuelo ventanaInformacionVuelo;
    private DateTimePicker datePicker;
    private Date date;

    public VueloController(VentanaVuelo vueloView, VentanaInformacionVuelo ventanaInformacionVuelo){
        datePicker = vueloView.getDateTimePicker();
        this.vueloView = vueloView;
        this.ventanaInformacionVuelo = ventanaInformacionVuelo;
        this.date = new Date();
    }



    public void actionPerformed(ActionEvent event) {

        if(event.getActionCommand().equals("Aceptar")){

            String referenciaVuelo = vueloView.getJtfVuelo().getText();
            String estadoVuelo = (String) vueloView.getJcbEstado().getSelectedItem();
            String referenciaDestino = vueloView.getJtfReferenciaD().getText();
            String referenciaOrigen = vueloView.getJtfReferenciaO().getText();
            String referenciaAvion = vueloView.getJtfRefeAvion().getText();
            int duracion = (int) vueloView.getJtfDuracion().getValue();
            //int retraso = (int) vueloView.getJtfRetardo().getValue();
            int pincode = (int) vueloView.getJtfPincode().getValue();

            Date fecha = datePicker.getDate();
            

            if (referenciaVuelo.equals("") || referenciaVuelo.length() >= 50 ){
                if(referenciaVuelo.equals("")){
                    JOptionPane.showMessageDialog(vueloView,
                            "La referencia no puede estar vacia.",
                            "Input error",
                            JOptionPane.ERROR_MESSAGE);
                }else {
                    JOptionPane.showMessageDialog(vueloView,
                            "La referencia no puede contener mas de 50 caracteres.",
                            "Input error",
                            JOptionPane.ERROR_MESSAGE);
                }
            }else if (referenciaDestino.equals("") || referenciaDestino.length() >= 50 ){
                if(referenciaDestino.equals("")){
                    JOptionPane.showMessageDialog(vueloView,
                            "La referencia no puede estar vacia.",
                            "Input error",
                            JOptionPane.ERROR_MESSAGE);
                }else {
                    JOptionPane.showMessageDialog(vueloView,
                            "La referencia no puede contener mas de 50 caracteres.",
                            "Input error",
                            JOptionPane.ERROR_MESSAGE);
                }
            }else if (referenciaOrigen.equals("") || referenciaOrigen.length() >= 50 ) {
                if (referenciaOrigen.equals("")) {
                    JOptionPane.showMessageDialog(vueloView,
                            "La referencia no puede estar vacia.",
                            "Input error",
                            JOptionPane.ERROR_MESSAGE);
                } else {
                    JOptionPane.showMessageDialog(vueloView,
                            "La referencia no puede contener mas de 50 caracteres.",
                            "Input error",
                            JOptionPane.ERROR_MESSAGE);
                }
            }else if (referenciaAvion.equals("") || referenciaAvion.length() >= 50 ) {
                if (referenciaAvion.equals("")) {
                    JOptionPane.showMessageDialog(vueloView,
                            "La referencia no puede estar vacia.",
                            "Input error",
                            JOptionPane.ERROR_MESSAGE);
                } else {
                    JOptionPane.showMessageDialog(vueloView,
                            "La referencia no puede contener mas de 50 caracteres.",
                            "Input error",
                            JOptionPane.ERROR_MESSAGE);
                }
            }else{

                ConectorDB conn = ConectorDB.getInstance();
                int aux =  conn.newDbVuelo(referenciaVuelo,fecha,duracion,0,estadoVuelo,referenciaAvion,referenciaOrigen,referenciaDestino,pincode);
                ventanaInformacionVuelo.actualizaTablaVuelos();
                if(aux == 1){
                    JOptionPane.showMessageDialog(vueloView,
                            "El Avion ya tiene un vuelo asignado para ese día.",
                            "Input error",
                            JOptionPane.ERROR_MESSAGE);
                }else if(aux == 2){
                    JOptionPane.showMessageDialog(vueloView,
                            "Ya existe un vuelo con esa referencia.",
                            "Input error",
                            JOptionPane.ERROR_MESSAGE);
                }else if(aux == 3){
                    JOptionPane.showMessageDialog(vueloView,
                            "Error al conectarse con la BBDD, porfavor intentelo mas tarde o actualize la cuenta a version premium para disfrutar de acceso ilimitado.",
                            "Input error",
                            JOptionPane.ERROR_MESSAGE);
                }

            }


        }else if(event.getActionCommand().equals("Eliminar")){

            String referencia = vueloView.getJtfVuelo().getText();

            if (referencia.equals("") || referencia.length() >= 50 ){
                if(referencia.equals("")){
                    JOptionPane.showMessageDialog(vueloView,
                            "La referencia no puede estar vacia.",
                            "Input error",
                            JOptionPane.ERROR_MESSAGE);
                }else {
                    JOptionPane.showMessageDialog(vueloView,
                            "La referencia no puede contener mas de 50 caracteres.",
                            "Input error",
                            JOptionPane.ERROR_MESSAGE);
                }
            }else{
                //ELIMINAR VUELO
                ConectorDB conn = ConectorDB.getInstance();
                if(conn.existeDbVuelo(referencia)){
                    conn.deleteDbVuelo(referencia);
                    ventanaInformacionVuelo.actualizaTablaVuelos();
                    JOptionPane.showMessageDialog(vueloView,
                            "El Avion deseado se ha decomisionado correctamente.",
                            "Input error",
                            JOptionPane.OK_OPTION);
                }else{
                    JOptionPane.showMessageDialog(vueloView,
                            "La referencia no existe.",
                            "Input error",
                            JOptionPane.ERROR_MESSAGE);
                }
            }
        }
    }
}
