package network;

import view.ViewServidor;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * Created by Toni on 03/05/2017.
 */
public class ServerClientTCPController extends Thread{
    private ViewServidor viewServidor;
    private boolean isOn;
    private ServerSocket sSocket;
    private ClientTCPController clientTCPController;
    private NetworkConfiguration networkConfiguration;

    public ServerClientTCPController(ViewServidor viewServidor){
        try {
            this.isOn = false;
            this.viewServidor = viewServidor;
            this.networkConfiguration = new NetworkConfiguration();
            // obrim un socket de tipus servidor
            this.sSocket = new ServerSocket(networkConfiguration.getPORT_CLIENT());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void run() {
        while (isOn) {
            try {
                //RESTA A L'ESPERA DE NOUS CLIENTS
                Socket sClient = sSocket.accept(); // BLOQUEJA EXECUCIO DEL THREAD fins nou Client

                clientTCPController = new ClientTCPController(sClient, viewServidor);   //Creem un nou client
                clientTCPController.startClientConnection();    //Comença a córrer el client

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void startClientServer() {
        // iniciem el thread del servidor
        isOn = true;
        this.start();
    }

    public void stopClientServer() {
        // aturem el thread del servidor
        isOn = false;
        this.interrupt();
    }
}

