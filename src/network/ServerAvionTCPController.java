package network;
import database.ConectorDB;
import view.VentanaInformacionVuelo;
import view.ViewServidor;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;


/**
 * Created by Anton
 */


public class ServerAvionTCPController extends Thread {

    private ViewServidor viewServidor;
    private ServerSocket sServer;
    private AvionTCPController fcmAvion;

    private NetworkConfiguration networkConfiguration;
    private VentanaInformacionVuelo ventanaInformacionVuelo;


    private int nextPort;
    private Socket sClient;
    private DataInputStream diStream;
    private DataOutputStream doStream;

    public ServerAvionTCPController(ViewServidor viewServidor, VentanaInformacionVuelo ventanaInformacionVuelo) {
        this.networkConfiguration = new NetworkConfiguration();
        this.viewServidor=viewServidor;
        this.nextPort = networkConfiguration.getPORT_AVION()+1;
        this.ventanaInformacionVuelo = ventanaInformacionVuelo;
    }


    public void run(){

        try {
            sServer = new ServerSocket(networkConfiguration.getPORT_AVION());

            while (true){
                try {
                    sClient = sServer.accept();
                    this.diStream = new DataInputStream(sClient.getInputStream());
                    this.doStream = new DataOutputStream(sClient.getOutputStream());
                    String opcion = diStream.readUTF();
                    if (opcion.equals("LOGIN")) {
                        String referenciaAvion = diStream.readUTF();
                        int pincodeVuelo = diStream.readInt();
                        String referenciaVuelo = diStream.readUTF();
                        ConectorDB conn = ConectorDB.getInstance();
                        if (conn.loginAvion(referenciaAvion, pincodeVuelo, referenciaVuelo)) {
                            this.doStream.writeBoolean(true);
                            doStream.writeInt(nextPort);
                            conn.setLoginFlag(referenciaVuelo);

                            fcmAvion = new AvionTCPController(this, referenciaAvion, pincodeVuelo, nextPort, referenciaVuelo, ventanaInformacionVuelo);
                            fcmAvion.start();
                            nextPort++;
                        } else {
                            doStream.writeBoolean(false);
                        }
                    }
                }catch (IOException e){
                    e.printStackTrace();
                    continue;
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
