package network;


import database.ConectorDB;
import model.EstadoAvion;
import view.VentanaInformacionVuelo;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * Created by Anton
 */

public class AvionTCPController extends Thread{

    private ServerAvionTCPController fcmService;
    private ServerSocket sServer;
    private Socket sClient;
    private DataInputStream diStream;
    private DataOutputStream doStream;
    private ObjectOutputStream objectOutputStream;
    private ObjectInputStream objectInputStream;
    private boolean active;
    private VentanaInformacionVuelo ventanaInformacionVuelo;

    private int port;
    private String referenciaAvion, referenciaVuelo, estadoVuelo;
    private int pincode, retraso;

    public AvionTCPController(ServerAvionTCPController fcmService, String referenciaAvion, int pincode, int port, String referenciaVuelo, VentanaInformacionVuelo ventanaInformacionVuelo) throws IOException {
        this.port = port;
        this.fcmService = fcmService;
        this.sServer = new ServerSocket(port);
        this.referenciaAvion = referenciaAvion;
        this.pincode = pincode;
        this.referenciaVuelo = referenciaVuelo;
        active = true;
        this.ventanaInformacionVuelo = ventanaInformacionVuelo;
    }


    public void run(){
        try {
            this.sClient = this.sServer.accept();
            this.diStream = new DataInputStream(sClient.getInputStream());
            this.doStream = new DataOutputStream(sClient.getOutputStream());
            this.objectOutputStream = new ObjectOutputStream(sClient.getOutputStream());
            this.objectInputStream = new ObjectInputStream(sClient.getInputStream());
            EstadoAvion estadoAvion = ConectorDB.getInstance().sendEstadoAvion(pincode, referenciaAvion, referenciaVuelo);
            objectOutputStream.writeObject(estadoAvion);
            ConectorDB conn = ConectorDB.getInstance();

            while(active){
                String opcion = diStream.readUTF();
                //System.out.println(opcion);

                switch (opcion){
                    case "LOGOUT":
                        doStream.writeBoolean(true);
                        sClient.close();
                        active = false;

                        break;
                    case "UPDATE":
                        doStream.writeBoolean(true);
                        referenciaVuelo = diStream.readUTF();
                        pincode = diStream.readInt();
                        if (conn.loginAvion(referenciaAvion, pincode, referenciaVuelo)) {
                            doStream.writeBoolean(true);
                            retraso = 0;
                            retraso = diStream.readInt();
                            if (retraso == 0) {
                                conn.updateEstadoVuelo("ON-TIME",referenciaVuelo);
                            }else{
                                conn.updateEstadoVuelo("DELAYED",referenciaVuelo);
                            }
                            conn.retrasoVuelo(referenciaVuelo, referenciaAvion, retraso);
                        } else {
                            doStream.writeBoolean(false);
                        }
                        ventanaInformacionVuelo.actualizaTablaVuelos();
                        break;

                    case "ESTADO":
                        doStream.writeBoolean(true);
                        estadoVuelo = diStream.readUTF();
                        conn.updateEstadoVuelo(estadoVuelo,referenciaVuelo);
                        break;

                    default:sClient.close();
                        break;
                }
            }
        } catch (IOException e) {
            //e.printStackTrace();
            try {
                sClient.close();
            } catch (IOException e1) {
                //e1.printStackTrace();
            }
        }
    }
}
