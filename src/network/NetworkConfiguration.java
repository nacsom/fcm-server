package network;

import com.google.gson.Gson;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

public class NetworkConfiguration {
	// constants relacionades amb la comunicacio

	private String  SERVER_IP;
	private String PORT_CLIENT;
	private String PORT_AVION;

	private String USERNAME;
	private String PASSWORD;
	private String DB;
	private int PORT;

	public NetworkConfiguration(){
		ArrayList<String> arrayList = readFile();
		this.SERVER_IP = arrayList.get(0);
		this.PORT_CLIENT = arrayList.get(1);
		this.PORT_AVION = arrayList.get(2);
		this.USERNAME = arrayList.get(3);
		this.PASSWORD = arrayList.get(4);
		this.DB = arrayList.get(5);
		this.PORT = Integer.valueOf(arrayList.get(6));
	}

	public ArrayList<String> readFile(){
		Gson gson = new Gson();
		try {
			BufferedReader br = new BufferedReader(new FileReader("lib/config.json"));
			ArrayList<String> arrayList;
			arrayList = gson.fromJson(br, ArrayList.class);
			return arrayList;
		}catch (IOException e){
			e.printStackTrace();
		}
		return null;
	}

	public String getSERVER_IP() {
		return SERVER_IP;
	}

	public void setSERVER_IP(String SERVER_IP) {
		this.SERVER_IP = SERVER_IP;
	}

	public int getPORT_CLIENT() {
		return Integer.valueOf(PORT_CLIENT);
	}

	public void setPORT_CLIENT(String PORT_CLIENT) {
		this.PORT_CLIENT = PORT_CLIENT;
	}

	public int getPORT_AVION() {
		return Integer.parseInt(PORT_AVION);
	}

	public void setPORT_AVION(String PORT_AVION) {
		this.PORT_AVION = PORT_AVION;
	}

	public String getUSERNAME() {
		return USERNAME;
	}

	public void setUSERNAME(String USERNAME) {
		this.USERNAME = USERNAME;
	}

	public String getPASSWORD() {
		return PASSWORD;
	}

	public void setPASSWORD(String PASSWORD) {
		this.PASSWORD = PASSWORD;
	}

	public String getDB() {
		return DB;
	}

	public void setDB(String DB) {
		this.DB = DB;
	}

	public int getPORT() {
		return PORT;
	}

	public void setPORT(int PORT) {
		this.PORT = PORT;
	}
}