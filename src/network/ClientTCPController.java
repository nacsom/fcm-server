package network;

import database.ConectorDB;
import model.Aeropuerto;
import model.Billete;
import model.Vuelo;
import view.ViewServidor;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.LinkedList;

/**
 * Created by Toni on 03/05/2017.
 */
/*
ESTO ES UN SERVIDOR DEDICADO A CLIENTE
 */

public class ClientTCPController extends Thread {
    private boolean isOn;
    private Socket sClient;

    private DataInputStream dataIn;
    private DataOutputStream dataOut;
    private ObjectOutputStream objectOut;

    private ViewServidor viewServidor;
    private ConectorDB conn;

    public ClientTCPController(Socket sClient, ViewServidor viewServidor) throws IOException {
        this.isOn = false;
        this.sClient = sClient;
        this.viewServidor = viewServidor;
    }

    public void run() {
        try {
            // creem els canals de comunicacio
            dataOut = new DataOutputStream(sClient.getOutputStream());
            dataIn = new DataInputStream(sClient.getInputStream());
            objectOut = new ObjectOutputStream(sClient.getOutputStream());

            conn = ConectorDB.getInstance();
            while (isOn) {  //Mientras no se apague, sigue la comunicación entre SERVER-CLIENTE
                String option = dataIn.readUTF();

                switch (option) {
                    case "WANT_TO_LOGIN":
                        wantToLogin();
                        break;
                    case "WANT_TO_REGISTER":
                        wantToRegister();
                        break;
                    case "WANT_AVAILABLE_AIRPORTS":
                        wantAvailableAirports();
                        break;
                    case "WANT_10_FLIGHTS":
                        want10Flights();
                        break;
                    case "WANT_BILLETES_USER":
                        wantBilletesUser();
                        break;
                    case "WANT_SHOW_FLIGHT_STATUS":
                        wantShowFlightStatus();
                        break;
                    case "WANT_TO_BUY_BILLETE":
                        wantToBuyBillete();
                        break;
                    case "PETICIO_BUY_BILLETE":
                        peticioBuyBillete();
                        break;
                    case "WANT_TO_DISCONNECT":
                        this.stopClientConnection();
                        break;
                }
            }
        } catch(IOException e){
                stopClientConnection();
                // eliminem el servidor dedicat del conjunt de servidors dedicats
        }
    }

    public void startClientConnection() {
        // iniciem el servidor dedicat
        isOn = true;
        this.start();
    }

    public void stopClientConnection() {
        // aturem el servidor dedicat
        this.isOn = false;
        this.interrupt();
    }

    void wantToLogin() throws IOException {
        String username = dataIn.readUTF();
        String password = dataIn.readUTF();
        System.out.println("US: " + username);
        System.out.println("PSW: " + password);
        if(conn.loginUSER(username, password)){
            System.out.println("OKAY");
            dataOut.writeUTF("OK");
        }else{
            System.out.println("FAIL");
            dataOut.writeUTF("KO");
        }
    }

    public void wantToRegister() throws IOException {
        String username = dataIn.readUTF();
        String password = dataIn.readUTF();
        String email = dataIn.readUTF();
        String nom = dataIn.readUTF();
        String apellido = dataIn.readUTF();
        if(!conn.existeUsuario(username)){
            conn.newDbUsuario(username, password, nom, apellido, email);
            dataOut.writeUTF("OK");
        }else{
            dataOut.writeUTF("KO");
        }
    }

    public void wantAvailableAirports() throws IOException {
        LinkedList<Aeropuerto> airports = conn.getAeropuertosDisponibles();
        LinkedList<String> airportsName = new LinkedList<>();
        int numberOfAirports = airports.size();
        for(int i = 0; i < numberOfAirports; i++){
            airportsName.add(airports.get(i).getNom());
        }
        objectOut.writeObject(airportsName);
        //Enviamos al Cliente para que actualice en su ComboBox el nombre de los aeropuertos
    }

    public void want10Flights() throws IOException {
        LinkedList<Vuelo> flights = new LinkedList<>();
        String airportName;
        airportName = dataIn.readUTF();     //Llegim l'aeroport del qual volen els vols
        flights = conn.get10UltimVuelos(airportName);
        dataOut.writeInt(flights.size());   //Necessita saber quants vols li passarem (poden ser menys que 10)
        int numberOfFlights = flights.size();
        for(int i = 0; i < numberOfFlights; i++){
            dataOut.writeUTF(flights.get(i).getReferencia());
            dataOut.writeInt(flights.get(i).getPlazasDisponibles());
            dataOut.writeUTF(flights.get(i).getFechaSalida());
        }
    }

    private void wantBilletesUser() throws IOException {
        String username;
        username = dataIn.readUTF();
        LinkedList<Billete> billetes = conn.billetesUsuario(username);
        int numberOfBillets = billetes.size();
        dataOut.writeInt(numberOfBillets);
        for(int i = 0; i < numberOfBillets; i++){
            dataOut.writeUTF(billetes.get(i).getReferencia());
            dataOut.writeUTF(billetes.get(i).getReferenciaUsuario());
            dataOut.writeUTF(billetes.get(i).getReferenciaVuelo());
        }
    }

    private void wantShowFlightStatus() throws IOException {
        String referenciaVuelo = dataIn.readUTF();
        System.out.println("REF: " + referenciaVuelo);
        Vuelo vuelo = conn.getVuelo(referenciaVuelo);
        dataOut.writeUTF(vuelo.getReferencia());
        dataOut.writeUTF(vuelo.getFechaSalida());
        dataOut.writeInt(vuelo.getDuracion());
        dataOut.writeInt(vuelo.getRetraso());
        dataOut.writeUTF(vuelo.getEstado());
        dataOut.writeUTF(vuelo.getReferenciaAvion());
        dataOut.writeUTF(vuelo.getReferenciaOrigen());
        dataOut.writeUTF(vuelo.getGetReferenciaDestina());
    }


    private void wantToBuyBillete() throws IOException {
        String aeropuertoOrigen = dataIn.readUTF();
        String aeropuertoDestino = dataIn.readUTF();
        String fechaSalida = dataIn.readUTF();
        LinkedList<Vuelo> vuelos = conn.vuelosOrigenDestionDisponibles(aeropuertoOrigen, aeropuertoDestino, fechaSalida);
        System.out.println(vuelos.toString());
        sendVuelos(vuelos);
        System.out.println("vuelos: " + vuelos.size());
        //dataOut.writeInt(vuelos.size());
        //objectOut.writeObject(vuelos.get(0));
    }

    public void sendVuelos(LinkedList<Vuelo> vuelos){
        try {
            int size = vuelos.size();
            dataOut.writeInt(size);
            for(int i = 0; i < size; i++){
                dataOut.writeUTF(vuelos.get(i).getReferencia());
                dataOut.writeUTF(vuelos.get(i).getFechaSalida());
                dataOut.writeInt(vuelos.get(i).getDuracion());
                dataOut.writeInt(vuelos.get(i).getRetraso());
                dataOut.writeUTF(vuelos.get(i).getEstado());
                dataOut.writeUTF(vuelos.get(i).getReferenciaAvion());
                dataOut.writeUTF(vuelos.get(i).getReferenciaOrigen());
                dataOut.writeUTF(vuelos.get(i).getGetReferenciaDestina());
                dataOut.writeInt(vuelos.get(i).getPlazasDisponibles());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void peticioBuyBillete(){
        try {
            String referencia = dataIn.readUTF();
            String username = dataIn.readUTF();
            int numeroBitllets = dataIn.readInt();
            String answer = conn.compraBillete(referencia, username, numeroBitllets);
            dataOut.writeUTF(answer);       //RESPONEM AMB OK / KO
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
