package database;

import com.mysql.jdbc.Connection;
import com.mysql.jdbc.Statement;
import model.*;
import network.NetworkConfiguration;

import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedList;


public final class ConectorDB {
    String userName;
    String password;
    String db;
    int port;
    String url = "jdbc:mysql://localhost";
    Connection conn = null;
    Statement s;
    NetworkConfiguration networkConfiguration;
    public static ConectorDB instance;


    private ConectorDB() {
        this.networkConfiguration = new NetworkConfiguration();     //Leemos fichero con información para login a BDD
        this.userName = networkConfiguration.getUSERNAME();
        this.password = networkConfiguration.getPASSWORD();
        this.db = networkConfiguration.getDB();
        this.port = networkConfiguration.getPORT();
        url += ":"+port+"/";
        //ConectorDB.url += ":"+port+";DatabaseName=";
        url += db;
        try {
            Class.forName("com.mysql.jdbc.Driver");
            conn = (Connection) DriverManager.getConnection(url, userName, password);

            if (conn != null) {
                System.out.println("Conexión a base de dades "+url+" ... Ok");
            }
        }
        catch(SQLException ex) {
            System.out.println("Problema al connecta-nos a la BBDD --> "+url);
        }
        catch(ClassNotFoundException ex) {
            System.out.println(ex);
        }
    }

    public static synchronized ConectorDB getInstance() {
        if ( instance == null ) {
            instance = new ConectorDB();
        }
        return instance;
    }

    public synchronized void disconnectInstance(){
        try {
            conn.close();
        } catch (SQLException e) {
            System.out.println("Problema al tancar la connexi� --> " + e.getSQLState());
        }
    }





    public void insertQuery(String query){
        try {
            s =(Statement) conn.createStatement();
            s.executeUpdate(query);

        } catch (SQLException ex) {
            System.out.println("Problema al Inserir --> " + ex.getSQLState() + " " + ex.getLocalizedMessage());
        }
    }

    public void updateQuery(String query){
        try {
            s =(Statement) conn.createStatement();
            s.executeUpdate(query);

        } catch (SQLException ex) {
            System.out.println("Problema al Modificar --> " + ex.getSQLState());
        }
    }

    public void deleteQuery(String query){
        try {
            s =(Statement) conn.createStatement();
            s.executeUpdate(query);

        } catch (SQLException ex) {
            System.out.println("Problema al Eliminar --> " + ex.getSQLState());
        }

    }

    public ResultSet selectQuery(String query){
        ResultSet rs = null;
        try {
            s =(Statement) conn.createStatement();
            rs = s.executeQuery (query);

        } catch (SQLException ex) {
            System.out.println("Problema al Recuperar les dades --> " + ex.getSQLState());
        }
        return rs;
    }





    //CONEXIONES AVION

    public boolean existeDbAvion (String referencia){
        ResultSet consulta;
        boolean estado = false;
        consulta = instance.selectQuery("SELECT 1 FROM aviones WHERE referencia ='"+referencia+"'");
        try {
            if(consulta.next()){
                estado = true;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return estado;
    }
    public void deleteDbAvion (String referencia){

        instance.deleteQuery("DELETE FROM aviones WHERE referencia ='"+referencia+"'");

    }

    //Procedimiento que crea un nuevo Avion en la base de datos
    public void newDbAvion (String referencia, String modelo, int plazas, int altura){

        instance.insertQuery("INSERT INTO aviones (referencia,modelo,plazas,altura) " +
                "VALUES ('"+referencia+"','"+modelo+"','"+plazas+"','"+altura+"')");

    }

    //Funcion que retorna el modelo del Avion deseado

    public String getModeloAvion(String referenciaAvion){

        ResultSet consulta;
        String modeloAvion = "";

        consulta = instance.selectQuery("SELECT modelo FROM aviones WHERE `referencia` = '"+referenciaAvion+"'");

        try {
            while (consulta.next()){
                modeloAvion = consulta.getString("modelo");
            }
        } catch (SQLException e) {
            System.out.println("Error no hay mas Next" + e);
        }

        return modeloAvion;
    }

    public int getPlazasAvion(String referenciaAvion) {

        ResultSet consulta;
        int plazas = 0;

        System.out.println(referenciaAvion);

        consulta = instance.selectQuery("SELECT plazas FROM aviones WHERE `referencia` = '"+referenciaAvion+"'");

        try {
            while (consulta.next()){
                plazas = consulta.getInt("plazas");
            }
        } catch (SQLException e) {
            System.out.println("Error no hay mas Next de plazas" + e);
        }

        return plazas;
    }
    public LinkedList<Avion> getAvionesDisponibles(){

        LinkedList<Avion> aviones = new LinkedList<Avion>();
        ResultSet consulta;
        Avion avion;

        consulta = (ResultSet) instance.selectQuery("SELECT * FROM aviones");

        try {
            while (consulta.next()){
                avion = new Avion(consulta.getString("referencia"),consulta.getString("modelo"),
                        consulta.getInt("plazas"),consulta.getInt("altura"));
                aviones.add(avion);
            }
        } catch (SQLException e) {
            System.out.println("Error no hay mas Next" + e);
        }

        return aviones;
    }














    //CONEXIONES AEROPUERTO

    public boolean existeDbAeropuerto (String referencia){
        ResultSet consulta;
        boolean estado = false;
        consulta = instance.selectQuery("SELECT 1 FROM aeropuertos WHERE referencia ='"+referencia+"'");
        try {
            if(consulta.next()){
                estado = true;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return estado;
    }

    //Procedimiento que crea un nuevo aeropuerto en la base de datos
    public void newDbAeropuerto (String referencia, String nom, int coordx, int coordy){

        instance.insertQuery("INSERT INTO aeropuertos (referencia,nom,coordx,coordy) " +
                "VALUES ('"+referencia+"','"+nom+"','"+coordx+"','"+coordy+"')");
    }

    //Funcion que retorna la referencia del aeropuerto
    public String getReferenciaAeropuerto(String nomAeropuerto) {

        String referenciaAeropuerto ="";
        ResultSet consulta;

        consulta = instance.selectQuery("SELECT referencia FROM aeropuertos WHERE `nom` = '"+nomAeropuerto+"'");

        try {
            while (consulta.next()){
                referenciaAeropuerto = consulta.getString("referencia");
            }
        } catch (SQLException e) {
            System.out.println("Error no hay mas Next" + e);
        }

        return referenciaAeropuerto;

    }


    public void deleteDbAeropuerto (String referencia){

        instance.deleteQuery("DELETE FROM aeropuertos WHERE referencia ='"+referencia+"'");

    }
    public LinkedList<Aeropuerto> getAeropuertosDisponibles(){

        LinkedList<Aeropuerto> aeropuertos = new LinkedList<Aeropuerto>();
        ResultSet consulta;
        Aeropuerto aeropuerto;



        consulta = (ResultSet) instance.selectQuery("SELECT * FROM aeropuertos");

        try {
            while (consulta.next()){
                aeropuerto = new Aeropuerto(consulta.getString("referencia"),consulta.getString("nom"),
                        consulta.getInt("coordx"),consulta.getInt("coordy"));
                aeropuertos.add(aeropuerto);
            }
        } catch (SQLException e) {
            System.out.println("Error no hay mas Next" + e);
        }


        return aeropuertos;
    }








    //CONEXIONES BILLETE

    //Procedimiento que crea un nuevo billete en la BBDD
    public void newDbBillete(String referencia, String referenciaUsuario, String referenciaVuelo){

        instance.insertQuery("INSERT INTO billetes (referencia,referenciaUsuario,referenciaVuelo)" +
                "VALUES ('"+referencia+"','"+referenciaUsuario+"','"+referenciaVuelo+"')");

        instance.insertQuery("INSERT INTO usuariobilletes (referenciaUsuario,referenciaBillete)" +
                "VALUES ('"+referenciaUsuario+"','"+referencia+"')");

    }
    public int numDbBilletes(){
        ResultSet consulta;
        int num = 0;

        consulta = instance.selectQuery("SELECT referencia FROM billetes ");

        try {
            while (consulta.next()){
                num++;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return num;
    }





    //CONEXIONES USUARIO

    public int numDbUsuarios(){
        ResultSet consulta;
        int num = 0;

        consulta = instance.selectQuery("SELECT username FROM usuarios ");

        try {
            while (consulta.next()){
                num++;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return num;
    }



    //Procedimiento que crea un nuevo usuarui en la BBDD
    public void newDbUsuario(String username, String password, String nom, String apellido, String email){


        instance.insertQuery("INSERT INTO usuarios (username,password,nom,apellido,email) " +
                "VALUES ('"+username+"','"+password+"','"+nom+"','"+apellido+"','"+email+"')");

    }

    //Retorna true si existe usuario (referencia unica y false si no existe usuariio)
    public Boolean existeUsuario(String username){
        String usernameAux = "";
        ResultSet consulta;

        consulta = instance.selectQuery("SELECT username FROM usuarios WHERE `username` = '"+username+"'");

        try {
            while (consulta.next()){
                usernameAux = consulta.getString("username");
            }
        } catch (SQLException e) {
            System.out.println("Error no hay mas Next" + e);
        }

        if(username.equals(usernameAux)){
            return true;
        }else{
            return false;
        }

    }




    //CONEXIONES VUELO


    //Procedimiento que crea un nuevo vuelo en la BBDD
    public int newDbVuelo(String referencia, Date aux, int duracion, int retraso, String estado,
                          String referenciaAvion, String referenciaOrigen, String referenciaDestino, int pincode){

        //String referenciaAeropuerto;
        String modeloAvion;
        int plazasDisponibles = 0;
        ResultSet consulta;

        // referenciaAeropuerto = getReferenciaAeropuerto(referenciaOrigen);
        java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        String fechaSalida =  sdf.format(aux);

        modeloAvion = getModeloAvion(referenciaAvion);
        plazasDisponibles = getPlazasAvion(referenciaAvion);

        consulta = instance.selectQuery("SELECT * FROM avionvuelos WHERE `referenciaAvion` = '"+referenciaAvion+"' AND DATE_FORMAT(fechaSalida, '%m-%d') = DATE_FORMAT('"+fechaSalida+"', '%m-%d')");

        try {
            if(consulta.next()){
                return 1;
            }else {
                consulta = instance.selectQuery("SELECT * FROM vuelos WHERE `referencia` = '"+referencia+"' ");
                try {
                    if (!consulta.next()) {
                        instance.insertQuery("INSERT INTO vuelos (referencia,pincode,fechaSalida,duracion,retraso,estado,referenciaAvion,referenciaOrigen,referenciaDestino,plazasDisponibles,modeloAvion)" +
                                "VALUES ('" + referencia + "','" + pincode + "','" + fechaSalida + "','" + duracion + "','" + retraso + "','" + estado + "','" + referenciaAvion + "','" + referenciaOrigen + "','" + referenciaDestino + "','" + plazasDisponibles + "','" + modeloAvion + "')");

                        instance.insertQuery("INSERT INTO aeropuertovuelos (referenciaAeropuerto,referenciaVuelo)" +
                                "VALUES ('" + referenciaOrigen + "','" + referencia + "')");

                        instance.insertQuery("INSERT INTO avionvuelos (referenciaAvion,referenciaVuelo,fechaSalida)" +
                                "VALUES ('" + referenciaAvion + "','" + referencia + "','" + fechaSalida + "')");
                    } else {
                        return 2;
                    }
                }catch (SQLException e){
                    return 3;
                }
            }
        } catch (SQLException e) {
            return 3;
        }
        return 0;
    }
    public boolean existeDbVuelo (String referencia){
        ResultSet consulta;
        boolean estado = false;
        consulta = instance.selectQuery("SELECT 1 FROM vuelos WHERE referencia ='"+referencia+"'");
        try {
            if(consulta.next()){
                estado = true;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return estado;
    }
    public void deleteDbVuelo (String referencia){

        instance.deleteQuery("DELETE FROM vuelos WHERE referencia ='"+referencia+"'");

    }


    //Funcion que actualiza el numero de plazas del vuelo
    public void updatePlazasVuelo(String referenciaVuelo, int numBilletes) {

        int plazasVuelo = 0;
        ResultSet consulta;

        consulta = instance.selectQuery("SELECT plazasDisponibles FROM vuelos WHERE `referencia` = '"+referenciaVuelo+"'");
        try {
            while (consulta.next()){
                plazasVuelo = consulta.getInt("plazasDisponibles") - numBilletes;
            }
        } catch (SQLException e) {
            System.out.println("Error no hay mas Next" + e);
        }

        instance.updateQuery("UPDATE vuelos SET plazasDisponibles='"+ plazasVuelo +"' WHERE referencia='"+referenciaVuelo+"'");

    }

    //funcion que retorna las plazas disponibles de un cuelo
    public int getPlazasDisponibles(String referenciaVuelo){

        ResultSet consulta;
        int plazasVuelo  = 0;

        consulta = instance.selectQuery("SELECT plazasDisponibles FROM vuelos WHERE `referencia` = '"+referenciaVuelo+"'");
        try {
            while (consulta.next()){
                plazasVuelo = consulta.getInt("plazasDisponibles");
            }
        } catch (SQLException e) {
            System.out.println("Error no hay mas Next" + e);
        }

        return plazasVuelo;
    }

    //Funcion que retorna la informacion del vuelo
    public Vuelo getVuelo(String referenciaVuelo) {

        Vuelo vuelo = new Vuelo();
        ResultSet consulta;

        consulta = instance.selectQuery("SELECT * FROM vuelos WHERE `referencia` = '" + referenciaVuelo + "'");
        try {
            while (consulta.next()) {
                vuelo = new Vuelo(consulta.getString("referencia"), consulta.getString("fechaSalida").substring(0,consulta.getString("fechaSalida").length() -2 ), consulta.getInt("duracion"),
                        consulta.getInt("retraso"), consulta.getString("estado"), consulta.getString("referenciaAvion"),
                        consulta.getString("referenciaOrigen"), consulta.getString("referenciaDestino"), consulta.getInt("plazasDisponibles"), consulta.getString("modeloAvion"), consulta.getInt("pincode"));
            }
        } catch (SQLException e) {
            System.out.println("Error no hay mas Next" + e);
        }

        return vuelo;
    }

    public LinkedList<Vuelo> getVuelosDbDisponibles(){

        LinkedList<Vuelo> vuelos = new LinkedList<Vuelo>();
        ResultSet consulta;
        Vuelo vuelo;

        consulta = (ResultSet) instance.selectQuery("SELECT * FROM vuelos ");

        try {
            while (consulta.next()){
                vuelo = new Vuelo(consulta.getString("referencia"),consulta.getString("fechaSalida").substring(0,consulta.getString("fechaSalida").length() -2 ),
                        consulta.getInt("duracion"),consulta.getInt("retraso"),consulta.getString("estado"),
                        consulta.getString("referenciaAvion"), consulta.getString("referenciaOrigen"), consulta.getString("referenciaDestino"), 0, null, 0, consulta.getInt("loginFlag"));
                vuelos.add(vuelo);
            }
        } catch (SQLException e) {
            System.out.println("Error no hay mas Next" + e);
        }

        return vuelos;
    }








    //FUNCIONES SERVIDOR_CLIENTE





    //Funcion que retorna todos los Aeropuerto existentes

    public int vuelosPendientes(){
        int cuantos = 0;
        ResultSet consulta;
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        consulta = instance.selectQuery("SELECT * FROM vuelos WHERE fechaSalida > '"+sdf.format(cal.getTime())+"'");
        try {
            while (consulta.next()){
                cuantos++;
            }
        } catch (SQLException e) {
            System.out.println("Error no hay mas Next" + e);
        }
        return cuantos;
    }

    public boolean loginUSER(String userName, String password){
        Boolean estado = false;
        ResultSet consulta;
        String auxPass = null;
        String user = null;
        String email = null;


        consulta = instance.selectQuery("SELECT * FROM usuarios WHERE `username` = '"+userName+"' OR `email` = '"+userName+"'");
        try {
            while (consulta.next()){
                user = consulta.getString("username");
                email = consulta.getString("email");
                auxPass = consulta.getString("password");
            }
        } catch (SQLException e) {
            System.out.println("Error no hay mas Next" + e);
        }

        if(user != null && auxPass != null && email != null){
            if(password.equals(auxPass)) estado = true;
        }
        return estado;
    }

    //Funcion encargada de coger los ultimos 10 vuelos del aeropuerto de origen
    public LinkedList<Vuelo> get10UltimVuelos(String nomAeropuerto){


        ResultSet consulta;
        String referenciaAeropuerto = "";
        Vuelo vuelo;
        LinkedList <Vuelo> vuelos = new LinkedList<Vuelo>();

        referenciaAeropuerto = getReferenciaAeropuerto(nomAeropuerto);

        Calendar cal = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        consulta = (ResultSet) instance.selectQuery("SELECT * FROM vuelos WHERE `referenciaOrigen` = '"+referenciaAeropuerto+"' AND  fechaSalida > '"+sdf.format(cal.getTime())+"' LIMIT 10");

        try {
            while (consulta.next()){
                vuelo = new Vuelo(consulta.getString("referencia"), consulta.getString("fechaSalida").substring(0,consulta.getString("fechaSalida").length() -2 ),consulta.getInt("duracion"),
                        consulta.getInt("retraso"),consulta.getString("estado"),consulta.getString("referenciaAvion"),
                        consulta.getString("referenciaOrigen"),consulta.getString("referenciaDestino"),consulta.getInt("plazasDisponibles"),consulta.getString("modeloAvion"),consulta.getInt("pincode"));
                vuelos.add(vuelo);
            }
        } catch (SQLException e) {
            System.out.println("Error no hay mas Next" + e);
        }

        return vuelos;
    }

    // Funcion que retorna todos los vuelos disponibles desde los aeropuertos desesados.
    // Peticion del cliente para Comprar Billete. Controlamos que exista al menos 1 plaza
    public LinkedList<Vuelo> vuelosOrigenDestionDisponibles (String nomOrigen, String nomDestino, String fechaSalida){

        Vuelo vueloDisponible;
        LinkedList <Vuelo> vuelosDispoibles = new LinkedList();
        LinkedList <Vuelo> vuelosDispoibles2 = new LinkedList();

        ResultSet consulta;

        String referenciaOrigen = getReferenciaAeropuerto(nomOrigen);
        String referenciaDestino = getReferenciaAeropuerto(nomDestino);
        java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        consulta = (ResultSet) instance.selectQuery("SELECT * FROM vuelos WHERE `referenciaOrigen` = '"+referenciaOrigen+"' AND `referenciaDestino` = '"+referenciaDestino+"' AND `plazasDisponibles` > 0 AND fechaSalida >= '"+fechaSalida+"' ORDER BY fechaSalida");

        try {
            if(!consulta.next()){
                consulta = (ResultSet) instance.selectQuery("SELECT * FROM vuelos WHERE `referenciaOrigen` = '"+referenciaOrigen+"' AND `referenciaDestino` = '"+referenciaDestino+"' AND `plazasDisponibles` > 0 AND (fechaSalida BETWEEN DATE_SUB('"+fechaSalida+"', INTERVAL 5 DAY) AND DATE_ADD('"+fechaSalida+"', INTERVAL 5 DAY)) ORDER BY fechaSalida");

                while(consulta.next()){
                    vueloDisponible =  new Vuelo(consulta.getString("referencia"), consulta.getString("fechaSalida").substring(0, consulta.getString("fechaSalida").length() - 2),consulta.getInt("duracion"),
                            consulta.getInt("retraso"),consulta.getString("estado"),consulta.getString("referenciaAvion"),
                            consulta.getString("referenciaOrigen"),consulta.getString("referenciaDestino"),consulta.getInt("plazasDisponibles"),consulta.getString("modeloAvion"),consulta.getInt("pincode"));
                    vuelosDispoibles.add(vueloDisponible);
                }

            }else {
                consulta.beforeFirst();
                while(consulta.next()) {
                    vueloDisponible = new Vuelo(consulta.getString("referencia"), consulta.getString("fechaSalida").substring(0, consulta.getString("fechaSalida").length() - 2), consulta.getInt("duracion"),
                            consulta.getInt("retraso"), consulta.getString("estado"), consulta.getString("referenciaAvion"),
                            consulta.getString("referenciaOrigen"), consulta.getString("referenciaDestino"), consulta.getInt("plazasDisponibles"), consulta.getString("modeloAvion"), consulta.getInt("pincode"));
                    vuelosDispoibles.add(vueloDisponible);
                }
            }

        } catch (SQLException e) {
            System.out.println("Error no hay mas Next" + e);
        }
        return vuelosDispoibles;

    }

    //Funcion que compra un Billete, genera el numero de referencia del billete y comprueba que se puedan comprar un billete del vuelo
    public String compraBillete(String referenciaVuelo,  String referenciaUsuario, int numBilletes){

        String estado = "KO";
        Billete billete = new Billete();



        String referencia = billete.crearReferencia(referenciaUsuario,referenciaUsuario);

        if(getPlazasDisponibles(referenciaVuelo) < numBilletes){
            estado = "KO";
        }else{
            newDbBillete(referencia,referenciaUsuario,referenciaVuelo);
            updatePlazasVuelo(referenciaVuelo, numBilletes);
            estado = "OK";
        }


        return estado;
    }

    //Funcion que retorna todos los billetes comprado por un usuario
    public LinkedList<Billete> billetesUsuario (String userName){

        LinkedList<Billete> billetes = new LinkedList<>();
        Billete billete;
        ResultSet consulta,consulta2;

        consulta = (ResultSet) instance.selectQuery("SELECT referenciaBillete FROM usuariobilletes WHERE `referenciaUsuario` = '"+userName+"'");

        try {
            while (consulta.next()){
                String refBillete = consulta.getString("referenciaBillete");
                consulta2 = (ResultSet) instance.selectQuery("SELECT * FROM billetes WHERE `referencia` = '"+refBillete+"'");
                try{
                    while (consulta2.next()){
                        billete = new Billete(consulta2.getString("referencia"),consulta2.getString("referenciaUsuario"),consulta2.getString("referenciaVuelo"));
                        billetes.add(billete);
                    }
                }catch (SQLException e){
                    System.out.println("Error no hay mas Next" + e);
                }
            }
        } catch (SQLException e) {
            System.out.println("Error no hay mas Next" + e);
        }

        return billetes;
    }















    //FUNCIONES SERVIDOR AVION


    //funcion que actualiza el retraso de un vuelo en la BBDD
    public void retrasoVuelo(String referenciaVuelo, String referenciaAvion, int tiempoRetraso){

        instance.updateQuery("UPDATE `vuelos` SET `retraso`="+tiempoRetraso+" WHERE `referencia` ='"+referenciaVuelo+"' AND `referenciaAvion` ='"+referenciaAvion+"'" );

    }


    public void updateEstadoVuelo (String estadoVuelo, String referenciaVuelo){

        instance.updateQuery("UPDATE `vuelos` SET `estado`='"+estadoVuelo+"' WHERE `referencia` ='"+referenciaVuelo+"'" );


    }


    public void setLoginFlag(String referenciaVuelo){
        instance.updateQuery("UPDATE `vuelos` SET `loginFlag`= 1 WHERE `referencia` ='"+referenciaVuelo+"'" );
    }

    // funcion que autentica que el avion esta vinculado al pincode. Retorna true si es correcto, false si no es correcto
    public boolean loginAvion(String referenciaAvion, int pincodeVuelo, String refVuelo){

        boolean estado = false;
        int pincode = 0;
        String auxRefAvion = null;
        String auxRefVuelo = null;
        ResultSet consulta;


        consulta = instance.selectQuery("SELECT * FROM vuelos WHERE `pincode` = '"+pincodeVuelo+"' AND `referenciaAvion` ='"+referenciaAvion+"' AND `referencia` = '"+refVuelo+"'" );
        try {
            if(consulta.next()){
                estado = true;
            }
        } catch (SQLException e) {
            System.out.println("Error no hay mas Next" + e);
        }
        return estado;

    }

    //FALTA MANDAR LISTADO DE USUARIOS DEL VUELO
    public EstadoAvion sendEstadoAvion(int pincodeVuelo, String referenciaAvion, String referenciaVuelo){

        EstadoAvion estadoAvion = null ;
        String estadoVuelo = null;
        int duracionVuelo = 0;
        int retrasoVuelo = 0;
        int plazasOcupadas = 0;
        String referenciaOrigen = null;
        String aeropuertoOrigen = null;
        String referenciaDestino = null;
        String aeropuertoDestino = null;
        int coordxOrigen = 0;
        int coordyOrigen = 0;
        int coordxDestino = 0;
        int coordyDestino = 0;
        int alturaCrucero = 0;
        Vuelo nextVuelo = null;
        String fechaSalida = null; //String currentTime = sdf.format(dt); java.util.Date dt = new java.util.Date();   java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        LinkedList<Usuario> usuarios = new LinkedList<>();

        ResultSet consulta, consulta2;

        consulta = instance.selectQuery("SELECT * FROM vuelos WHERE `pincode` = '"+pincodeVuelo+"' AND `referencia` ='"+referenciaVuelo+"'  AND `referenciaAvion` ='"+referenciaAvion+"'" );

        try {
            while(consulta.next()){
                referenciaVuelo = consulta.getString("referencia");
                estadoVuelo = consulta.getString("estado");
                duracionVuelo = consulta.getInt("duracion");
                retrasoVuelo = consulta.getInt("retraso");
                referenciaOrigen = consulta.getString("referenciaOrigen");
                referenciaDestino = consulta.getString("referenciaDestino");
                fechaSalida = consulta.getString("fechaSalida");
                //System.out.println("aux: "+aux);
                //java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                //  = sdf.format(aux);
            }

        } catch (SQLException e) {
            System.out.println("Error no hay mas Next" + e);
        }

        consulta = instance.selectQuery("SELECT * FROM aeropuertos WHERE `referencia` = '"+referenciaOrigen+"'" );
        try {
            while (consulta.next()){
                aeropuertoOrigen = consulta.getString("nom");
                coordxOrigen = consulta.getInt("coordx");
                coordyOrigen = consulta.getInt("coordy");
            }
        } catch (SQLException e) {
            System.out.println("Aki Error no hay mas Next" + e);
        }

        consulta = instance.selectQuery("SELECT * FROM aeropuertos WHERE `referencia` = '"+referenciaDestino+"'" );
        try {
            while (consulta.next()){
                aeropuertoDestino = consulta.getString("nom");
                coordxDestino = consulta.getInt("coordx");
                coordyDestino = consulta.getInt("coordy");
            }
        } catch (SQLException e) {
            System.out.println("Aki Error no hay mas Next" + e);
        }


        consulta = instance.selectQuery("SELECT referenciaUsuario FROM billetes WHERE `referenciaVuelo` = '"+referenciaVuelo+"'" );

        try {
            if(!consulta.isBeforeFirst()){
                Usuario user = new Usuario(null,null,null,null,null);
                usuarios.add(user);

            }else{
                while(consulta.next()){
                    String referenciaUsuario = consulta.getString("referenciaUsuario");
                    consulta2 = instance.selectQuery("SELECT * FROM usuarios WHERE `username` = '"+referenciaUsuario+"'" );
                    try {
                        while(consulta2.next()){
                            String nom = consulta2.getString("nom");
                            String apellido = consulta2.getString("apellido");
                            String email =  consulta2.getString("email");
                            Usuario user = new Usuario(null,null, nom, apellido, email);
                            usuarios.add(user);

                        }
                    } catch (SQLException e) {
                        System.out.println("Error no hay mas Next" + e);
                    }

                }


            }
        } catch (SQLException e) {
            System.out.println("Error no hay mas Next" + e);
        }

        consulta = instance.selectQuery("SELECT * FROM avionvuelos WHERE `referenciaAvion` = '"+referenciaAvion+"' AND DATE_FORMAT(`fechaSalida`, '%m-%d') > DATE_FORMAT('"+fechaSalida+"', '%m-%d') ORDER BY fechaSalida LIMIT 1");
        try {
            if( consulta == null ){
                nextVuelo = new Vuelo(null,null,0,0,null);
            }else{
                if(!consulta.next()){
                    nextVuelo = new Vuelo(null,null,0,0,null);
                }else{
                    String referenciaAux = consulta.getString("referenciaVuelo");
                   // System.out.println("referenciaVuelo: "+referenciaAux);
                    consulta = instance.selectQuery("SELECT * FROM billetes WHERE referenciaVuelo = '"+referenciaAux+"'" );
                    int ocupantes = 0;
                    while (consulta.next()){
                        ocupantes++;
                    }
                    consulta = instance.selectQuery("SELECT * FROM vuelos WHERE referencia = '"+referenciaAux+"'" );
                    if(consulta.next()){
                        nextVuelo = new Vuelo(consulta.getString("fechaSalida").substring(0,consulta.getString("fechaSalida").length() -2 ),consulta.getString("referenciaOrigen"),consulta.getString("referenciaDestino"),ocupantes);
                    }
                }
            }
            //System.out.println("nextV"+nextVuelo.toString());
        } catch (SQLException e) {
            e.printStackTrace();
        }

        consulta2 = instance.selectQuery("SELECT altura FROM aviones WHERE referencia ='"+referenciaAvion+"'");

        try {
            if(!(consulta2 == null)){
                while (consulta2.next()){
                    alturaCrucero = consulta2.getInt("altura");
                }
            }

        } catch (SQLException e) {
            System.out.println("Error no hay mas Next" + e);
        }


        estadoAvion = new EstadoAvion(referenciaVuelo,estadoVuelo,duracionVuelo,retrasoVuelo, plazasOcupadas, referenciaOrigen, aeropuertoOrigen,referenciaDestino,aeropuertoDestino,coordxOrigen,coordyOrigen,coordxDestino,coordyDestino,fechaSalida, usuarios, nextVuelo, alturaCrucero);
        return  estadoAvion;
    }

}