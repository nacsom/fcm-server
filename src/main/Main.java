package main;


import controllers.AeropuertoController;
import controllers.AvionController;
import controllers.MenuController;
import controllers.VueloController;
import network.ServerAvionTCPController;
import network.ServerClientTCPController;
import view.*;

import javax.swing.*;


public class Main {
	public static void main(String[] args) {
		// Start program using Swing Utilities to ensure the system has enough resources
		// to build the Window.
		SwingUtilities.invokeLater(new Runnable() {

        // Start program using Swing Utilities to ensure the system has enough resources
        // to build the Window.

            public void run() {
                VentanaVuelo ventanaVuelo = new VentanaVuelo();
                VentanaAvion ventanaAvion = new VentanaAvion();
                VentanaAeropuerto ventanaAeropuerto = new VentanaAeropuerto();
                VentanaVuelosEnCurso ventanaVuelosEnCurso = new VentanaVuelosEnCurso();

                VentanaEstadistica ventanaEstadistica= new VentanaEstadistica(ventanaVuelosEnCurso);
                VentanaInformacionVuelo ventanaInformacionVuelo = new VentanaInformacionVuelo();
                VentanaInformacionAvion ventanaInformacionAvion = new VentanaInformacionAvion();
                VentanaInformacionAeropuerto ventanaInformacionAeropuerto = new VentanaInformacionAeropuerto();


                ViewServidor window = new ViewServidor(ventanaAvion, ventanaVuelo, ventanaVuelosEnCurso, ventanaAeropuerto,ventanaInformacionAvion,ventanaInformacionVuelo, ventanaInformacionAeropuerto, ventanaEstadistica);
                // Create the Window.
                VueloController vueloController = new VueloController(ventanaVuelo, ventanaInformacionVuelo);
                ventanaVuelo.setController(vueloController);    //VUELO

                AvionController avionController = new AvionController(ventanaAvion, ventanaInformacionAvion);
                ventanaAvion.setController(avionController);    //AVIO

                AeropuertoController aeropuertoController = new AeropuertoController(ventanaAeropuerto, ventanaInformacionAeropuerto);
                ventanaAeropuerto.setController(aeropuertoController);  //AEROPUERTO


                MenuController controladorMenu = new MenuController(window);

                window.registerController(controladorMenu);
                window.setVisible(true);

                // CONTROL DE START STOP DEL SERVER
                ServerAvionTCPController serverAvionTCPController = new ServerAvionTCPController(window, ventanaInformacionVuelo);
                serverAvionTCPController.start();
                ServerClientTCPController serverClientTCPController = new ServerClientTCPController(window);
                serverClientTCPController.startClientServer();

            }
        });
	}
}