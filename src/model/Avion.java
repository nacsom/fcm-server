package model;

import database.ConectorDB;

import java.io.Serializable;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Clase Modelo Avion
 */

public class Avion implements Serializable{

    private String referencia;
    private String modelo;
    private int plazas;
    private int altura;

    public Avion(){}

    public Avion(String referencia, String modelo, int plazas, int altura) {
        this.referencia = referencia;
        this.modelo = modelo;
        this.plazas = plazas;
        this.altura = altura;
    }


    @Override
    public String toString() {
        return "Avion{" +
                "referencia='" + referencia + '\'' +
                ", modelo='" + modelo + '\'' +
                ", plazas=" + plazas +
                ", altura=" + altura +
                '}';
    }

    public String getReferencia() {
        return referencia;
    }

    public String getModelo() {
        return modelo;
    }

    public int getPlazas() {
        return plazas;
    }

    public int getAltura() {
        return altura;
    }
}
