package model;

import java.io.Serializable;
import java.util.Random;


/**
 * Clase Modelo Billete
 */
public class Billete implements Serializable {

    private String referencia;
    private String referenciaUsuario;
    private String referenciaVuelo;

    public Billete(){}

    public Billete(String referencia, String referenciaUsuario, String referenciaVuelo) {
        this.referencia = referencia;
        this.referenciaUsuario = referenciaUsuario;
        this.referenciaVuelo = referenciaVuelo;
    }



    @Override
    public String toString() {
        return "Billete{" +
                "referencia='" + referencia + '\'' +
                ", referenciaUsuario='" + referenciaUsuario + '\'' +
                ", referenciaVuelo='" + referenciaVuelo + '\'' +
                '}';
    }

    //Funcion que retorna un String que sera la referencia de billete unica
    public String crearReferencia(String referenciaUsuario, String referenciaVuelo) {
        Random r = new Random();
        return referenciaUsuario.charAt(0) + referenciaUsuario.charAt(1) + referenciaUsuario.charAt(2) +
                referenciaVuelo.charAt(0) + referenciaVuelo.charAt(1) + referenciaVuelo.charAt(2) +
                String.valueOf(Float.valueOf((referenciaUsuario.charAt(0) + referenciaUsuario.charAt(1) + referenciaUsuario.charAt(2) +
                referenciaVuelo.charAt(0) + referenciaVuelo.charAt(1) + referenciaVuelo.charAt(2) + r.nextInt())/r.nextInt()));
    }

    public String getReferencia() {
        return referencia;
    }

    public void setReferencia(String referencia) {
        this.referencia = referencia;
    }

    public String getReferenciaUsuario() {
        return referenciaUsuario;
    }

    public void setReferenciaUsuario(String referenciaUsuario) {
        this.referenciaUsuario = referenciaUsuario;
    }

    public String getReferenciaVuelo() {
        return referenciaVuelo;
    }

    public void setReferenciaVuelo(String referenciaVuelo) {
        this.referenciaVuelo = referenciaVuelo;
    }
}
