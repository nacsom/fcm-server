package model;



import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;


/**
 * Clase Modelo Vuelo
 */

public class Vuelo implements Serializable {

    static final long serialVersionUID = 40L;
    private String referencia;
    private String fechaSalida;
    private int duracion;
    private int retraso;
    private String estado;
    private String referenciaAvion;
    private String referenciaOrigen;
    private String referenciaDestino;
    private int plazasDisponibles;
    private String modeloAvion;
    private int pincode;
    private int loginFlag;


    public Vuelo() {}

    public Vuelo(String fechaSalida, String referenciaOrigen, String referenciaDestino, int plazasDisponibles) {
        this.fechaSalida = fechaSalida;
        this.referenciaOrigen = referenciaOrigen;
        this.referenciaDestino = referenciaDestino;
        this.plazasDisponibles = plazasDisponibles;
    }

    public Vuelo(String referencia, String fechaSalida, int duracion, int retraso, String estado,
                 String referenciaAvion, String referenciaOrigen, String referenciaDestino, int plazasDisponibles, String modeloAvion, int pincode) {
        this.referencia = referencia;
        this.fechaSalida = fechaSalida;
        this.duracion = duracion;
        this.retraso = retraso;
        this.estado = estado;
        this.referenciaAvion = referenciaAvion;
        this.referenciaOrigen = referenciaOrigen;
        this.referenciaDestino = referenciaDestino;
        this.plazasDisponibles = plazasDisponibles;
        this.modeloAvion = modeloAvion;
        this.pincode = pincode;
    }
    public Vuelo(String referencia, String fechaSalida, int duracion, int retraso, String estado,
                 String referenciaAvion, String referenciaOrigen, String referenciaDestino, int plazasDisponibles, String modeloAvion, int pincode,int loginFlag) {
        this.referencia = referencia;
        this.fechaSalida = fechaSalida;
        this.duracion = duracion;
        this.retraso = retraso;
        this.estado = estado;
        this.referenciaAvion = referenciaAvion;
        this.referenciaOrigen = referenciaOrigen;
        this.referenciaDestino = referenciaDestino;
        this.plazasDisponibles = plazasDisponibles;
        this.modeloAvion = modeloAvion;
        this.pincode = pincode;
        this.loginFlag = loginFlag;
    }

    public Vuelo(String referencia, String fechaSalida, int duracion, int retraso, String estado) {
        this.referencia = referencia;
        this.fechaSalida = fechaSalida;
        this.duracion =duracion;
        this.retraso = retraso;
        this.estado =estado;
    }

    @Override
    public String toString() {
        return "Vuelo{" +
                "referencia='" + referencia + '\'' +
                ", fechaSalida='" + fechaSalida + '\'' +
                ", duracion=" + duracion +
                ", retraso=" + retraso +
                ", estado='" + estado + '\'' +
                ", referenciaAvion='" + referenciaAvion + '\'' +
                ", referenciaOrigen='" + referenciaOrigen + '\'' +
                ", getReferenciaDestina='" + referenciaDestino + '\'' +
                ", plazasDisponibles=" + plazasDisponibles +
                '}';
    }

    private void writeObject(ObjectOutputStream stream){
        try {
            stream.writeObject(referencia);
            stream.writeObject(fechaSalida);
            stream.writeInt(duracion);
            stream.writeInt(retraso);
            stream.writeObject(estado);
            stream.writeObject(referenciaAvion);
            stream.writeObject(referenciaOrigen);
            stream.writeObject(referenciaDestino);
            stream.writeInt(plazasDisponibles);
            stream.writeInt(loginFlag);
        } catch (IOException e) {
            e.printStackTrace();
        }


    }

    private void readObject(ObjectInputStream stream){
        try {
            this.referencia = (String) stream.readObject();
            this.fechaSalida = (String) stream.readObject();
            this.duracion = stream.readInt();
            this.retraso = stream.readInt();
            this.estado = (String) stream.readObject();
            this.referenciaAvion = (String) stream.readObject();
            this.referenciaOrigen = (String) stream.readObject();
            this.referenciaDestino = (String) stream.readObject();
            this.plazasDisponibles = stream.readInt();
            this.loginFlag = stream.readInt();

        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String getReferencia() {
        return referencia;
    }

    public String getFechaSalida() {
        return fechaSalida;
    }

    public int getDuracion() {
        return duracion;
    }

    public int getRetraso() {
        return retraso;
    }

    public String getEstado() {
        return estado;
    }

    public String getReferenciaOrigen() {
        return referenciaOrigen;
    }

    public void setReferencia(String referencia) {
        this.referencia = referencia;
    }

    public void setFechaSalida(String fechaSalida) {
        this.fechaSalida = fechaSalida;
    }

    public void setDuracion(int duracion) {
        this.duracion = duracion;
    }

    public void setRetraso(int retraso) {
        this.retraso = retraso;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getReferenciaAvion() {
        return referenciaAvion;
    }

    public void setReferenciaAvion(String referenciaAvion) {
        this.referenciaAvion = referenciaAvion;
    }

    public void setReferenciaOrigen(String referenciaOrigen) {
        this.referenciaOrigen = referenciaOrigen;
    }

    public String getGetReferenciaDestina() {
        return referenciaDestino;
    }

    public void setGetReferenciaDestina(String getReferenciaDestina) {
        this.referenciaDestino = getReferenciaDestina;
    }

    public int getPlazasDisponibles() {
        return plazasDisponibles;
    }

    public void setPlazasDisponibles(int plazasDisponibles) {
        this.plazasDisponibles = plazasDisponibles;
    }

    public String getModeloAvion() {
        return modeloAvion;
    }

    public void setModeloAvion(String modeloAvion) {
        this.modeloAvion = modeloAvion;
    }

    public int getPincode() {
        return pincode;
    }

    public void setPincode(int pincode) {
        this.pincode = pincode;
    }

    public int getLoginFlag() {
        return loginFlag;
    }
}
