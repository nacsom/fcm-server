package model;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.LinkedList;

/**
 * Created by Anton
 * Clase encargada de pasar la informacion relacionada con la epticion del estado actual del avion
 */
public class EstadoAvion implements Serializable{

    static final long serialVersionUID = 42L;
    private LinkedList<Usuario> usuarios;
    private String referenciaVuelo;
    private String estadoVuelo;
    private int duracionVuelo;
    private int retrasoVuelo;
    private int plazasOcupadas;
    private String referenciaOrigen;
    private String aeropuertoOrigen;
    private String referenciaDestino;
    private String aeropuertoDestino;
    private int coordxOrigen;
    private int coordyOrigen;
    private int coordxDestino;
    private int coordyDestino;
    private String fechaSalida;
    private Vuelo vuelo;
    private int alturaCrucero;

    public EstadoAvion(String referenciaVuelo, String estadoVuelo, int duracionVuelo, int retrasoVuelo, int plazasOcupadas, String referenciaOrigen, String aeropuertoOrigen, String referenciaDestino, String aeropuertoDestino, int coordxOrigen, int coordyOrigen, int coordxDestino, int coordyDestino, String fechaSalida, LinkedList<Usuario> usuarios, Vuelo vuelo, int alturaCrucero) {
        this.referenciaVuelo = referenciaVuelo;
        this.estadoVuelo = estadoVuelo;
        this.duracionVuelo = duracionVuelo;
        this.retrasoVuelo = retrasoVuelo;
        this.plazasOcupadas = plazasOcupadas;
        this.referenciaOrigen = referenciaOrigen;
        this.aeropuertoOrigen = aeropuertoOrigen;
        this.referenciaDestino = referenciaDestino;
        this.aeropuertoDestino = aeropuertoDestino;
        this.coordxOrigen = coordxOrigen;
        this.coordyOrigen = coordyOrigen;
        this.coordxDestino = coordxDestino;
        this.coordyDestino = coordyDestino;
        this.fechaSalida = fechaSalida;
        this.usuarios = usuarios;
        this.vuelo = vuelo;
        this.alturaCrucero = alturaCrucero;
    }

    private void writeObject(ObjectOutputStream stream){
        try {
            stream.writeObject(referenciaVuelo);
            stream.writeObject(estadoVuelo);
            stream.writeInt(duracionVuelo);
            stream.writeInt(retrasoVuelo);
            stream.writeInt(plazasOcupadas);
            stream.writeObject(referenciaOrigen);
            stream.writeObject(aeropuertoOrigen);
            stream.writeObject(referenciaDestino);
            stream.writeObject(aeropuertoDestino);
            stream.writeInt(coordxOrigen);
            stream.writeInt(coordyOrigen);
            stream.writeInt(coordxDestino);
            stream.writeInt(coordyDestino);
            stream.writeObject(fechaSalida);
            stream.writeObject(usuarios);
            stream.writeObject(vuelo);
            stream.writeInt(alturaCrucero);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private void readObject(ObjectInputStream stream){

        try {
            this.referenciaVuelo =(String) stream.readObject();
            this.estadoVuelo = (String)stream.readObject();
            this.duracionVuelo =  stream.readInt();
            this.retrasoVuelo = stream.readInt();
            this.plazasOcupadas = stream.readInt();
            this.referenciaOrigen = (String) stream.readObject();
            this.aeropuertoOrigen = (String) stream.readObject();
            this.referenciaDestino = (String) stream.readObject();
            this.aeropuertoDestino = (String) stream.readObject();
            this.coordxOrigen = stream.readInt();
            this.coordyOrigen = stream.readInt();
            this.coordxDestino = stream.readInt();
            this.coordyDestino = stream.readInt();
            this.fechaSalida = (String) stream.readObject();
            this.usuarios =(LinkedList<Usuario>)  stream.readObject();
            this.vuelo =(Vuelo)  stream.readObject();
            this.alturaCrucero = stream.readInt();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

    }
}
