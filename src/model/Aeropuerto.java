package model;

import com.mysql.jdbc.ResultSet;
import database.ConectorDB;

import java.io.Serializable;
import java.sql.SQLException;

/**
 * Clase Modelo Aeropuerto
 */

public class Aeropuerto implements Serializable{

    private String referencia;
    private String nom;
    private int coordx;
    private int coordy;

    public Aeropuerto(){}

    public Aeropuerto(String referencia, String nom, int coordx, int coordy) {
        this.referencia = referencia;
        this.nom = nom;
        this.coordx = coordx;
        this.coordy = coordy;
    }


    @Override
    public String toString() {
        return "Aeropuerto{" +
                "referencia='" + referencia + '\'' +
                ", nom='" + nom + '\'' +
                ", coordx=" + coordx +
                ", coordy=" + coordy +
                '}';
    }

    public String getReferencia() {
        return referencia;
    }

    public String getNom() {
        return nom;
    }

    public int getCoordx() {
        return coordx;
    }

    public int getCoordy() {
        return coordy;
    }


  }

