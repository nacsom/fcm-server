package model;

import database.ConectorDB;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.sql.ResultSet;
import java.sql.SQLException;


/**
 * Clase Modelo Usuario
 */
public class Usuario implements Serializable{

    static final long serialVersionUID = 41L;
    private String username;
    private String password;
    private String nom;
    private String apellido;
    private String email;

    public Usuario(){}

    public Usuario(String username, String password, String nom, String apellido, String email) {
        this.username = username;
        this.password = password;
        this.nom = nom;
        this.apellido = apellido;
        this.email = email;
    }

    @Override
    public String toString() {
        return "Usuario{" +
                "username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", nom='" + nom + '\'' +
                ", apellido='" + apellido + '\'' +
                ", email='" + email + '\'' +
                '}';
    }

    private void writeObject(ObjectOutputStream stream){
        try {
            stream.writeObject(username);
            stream.writeObject(password);
            stream.writeObject(nom);
            stream.writeObject(apellido);
            stream.writeObject(email);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private void readObject(ObjectInputStream stream){
        try {
            this.username = (String) stream.readObject();
            this.password = (String) stream.readObject();
            this.nom = (String) stream.readObject();
            this.apellido = (String) stream.readObject();
            this.email = (String) stream.readObject();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

    }
}
