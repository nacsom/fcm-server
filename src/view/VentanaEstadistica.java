package view;


import database.ConectorDB;
import model.Aeropuerto;
import model.Vuelo;

import javax.swing.*;
import java.awt.*;
import java.util.LinkedList;


public class VentanaEstadistica extends JPanel implements Runnable {

     private JLabel jlNumeroDeAeropuertos;
     private JLabel jlNumeroDeVuelos;
     private JLabel jlNumeroDeVuelosEnTranscurso;
     private JLabel jlNumeroDeVuelosPendientes;
     private JLabel jlNumeroDeVuelosFinalizados;
     private JLabel  jlNumeroDeUsuarios;
     private JLabel jlNumeroDeBoletos;
     private JPanel jpVentanaEstadistica;
     private VentanaVuelosEnCurso ventanaVuelosEnCurso;
     private ConectorDB conn;
     private LinkedList<Aeropuerto> aeropuertos;
     private LinkedList<Vuelo> vuelos;

    private Thread thread;
    private int counter = 0;


    public VentanaEstadistica(VentanaVuelosEnCurso ventanaVuelosEnCurso) {

        conn = ConectorDB.getInstance();
        aeropuertos = conn.getAeropuertosDisponibles();
        vuelos = conn.getVuelosDbDisponibles();
        this.ventanaVuelosEnCurso = ventanaVuelosEnCurso;

        jpVentanaEstadistica = new JPanel(new GridLayout(7,1));

        jlNumeroDeAeropuertos = new JLabel("Numero de aeropuertos: " + aeropuertos.size());
        jlNumeroDeVuelos = new JLabel("Numero de vuelos: " + vuelos.size());
        jlNumeroDeVuelosEnTranscurso = new JLabel("Numero de vuelos en transcurso: " + ventanaVuelosEnCurso.getVuelosEnCurso());
        jlNumeroDeVuelosPendientes = new JLabel("Numero de vuelos pendientes: "+  conn.vuelosPendientes());
        jlNumeroDeVuelosFinalizados = new JLabel("Numero de vuelos finalizados :" + ventanaVuelosEnCurso.getVuelosFinalizados());
        jlNumeroDeUsuarios = new JLabel("Numero de usuarios: " + conn.numDbUsuarios());
        jlNumeroDeBoletos = new JLabel("Numero de Billetes: " + conn.numDbBilletes());

        jpVentanaEstadistica.add(jlNumeroDeAeropuertos);
        jpVentanaEstadistica.add(jlNumeroDeUsuarios);
        jpVentanaEstadistica.add(jlNumeroDeBoletos);
        jpVentanaEstadistica.add(jlNumeroDeVuelos);
        jpVentanaEstadistica.add(jlNumeroDeVuelosEnTranscurso);
        jpVentanaEstadistica.add(jlNumeroDeVuelosPendientes);
        jpVentanaEstadistica.add(jlNumeroDeVuelosFinalizados);

        this.add(jpVentanaEstadistica,BorderLayout.CENTER);
        this.setVisible(true);

       // (new Thread(new VentanaEstadistica(ventanaVuelosEnCurso))).start();

    }

    @Override
    public void addNotify() {
        super.addNotify();
        thread = new Thread(this);
        thread.start();
    }

    public void run() {
        long beforeTime, timeDiff, sleep;

        beforeTime = System.currentTimeMillis();
        while (true) {

            timeDiff = System.currentTimeMillis() - beforeTime;
            sleep = (long) (100 - timeDiff);

            if (sleep < 0)
                sleep = 2;
            try {
                Thread.sleep(sleep);
                counter++;
                if(counter == 3){   //Actualizacion de tablas cada 3 segundos..
                    actualizaEstadisticas();
                    counter = 0;
                }
            } catch (InterruptedException e) {
                System.out.println("interrupted");
            }

            beforeTime = System.currentTimeMillis();
        }
    }

    public void actualizaEstadisticas(){
        aeropuertos = conn.getAeropuertosDisponibles();
        vuelos = conn.getVuelosDbDisponibles();
        jlNumeroDeAeropuertos.setText("Numero de aeropuertos: " + aeropuertos.size());
        jlNumeroDeVuelos.setText("Numero de vuelos: " + vuelos.size());
        jlNumeroDeVuelosEnTranscurso.setText("Numero de vuelos en transcurso: " + ventanaVuelosEnCurso.getVuelosEnCurso());
        jlNumeroDeVuelosPendientes.setText("Numero de vuelos pendientes: "+  conn.vuelosPendientes());
        jlNumeroDeVuelosFinalizados.setText("Numero de vuelos finalizados: " + ventanaVuelosEnCurso.getVuelosFinalizados());
        jlNumeroDeUsuarios.setText("Numero de usuarios: " + conn.numDbUsuarios());
        jlNumeroDeBoletos.setText("Numero de Billetes: " + conn.numDbBilletes());
    }

}






