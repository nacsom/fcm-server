package view;

import controllers.AvionController;

import javax.swing.*;
import java.awt.*;


/**
 * Created by gabri_000 on 31/03/2017.
 */
public class VentanaAvion extends JPanel {

    private JButton jbAceptar;
    private JButton jbEliminar;

    private JPanel jpMenuA;
    private JPanel jpMenuA1;
    private JTextField jtfAvion;
    private JLabel jlAvion;
    private JTextField jtfModeloA;
    private JLabel jlModeloA;
    private JSpinner jtfPlazas;
    private JLabel jlPlazas;
    private JLabel jlAltura;
    private JSpinner jtfAltura;


    public VentanaAvion() {


        //JFrame de la opcion del avion

        jpMenuA = new JPanel(new GridLayout(4, 2));

        jtfAvion = new JTextField();
        jlAvion = new JLabel("Referencia ");
        jpMenuA.add(jlAvion);
        jpMenuA.add(jtfAvion);

        jtfModeloA = new JTextField();
        jlModeloA = new JLabel("Modelo del avion");
        jpMenuA.add(jlModeloA);
        jpMenuA.add(jtfModeloA);

        SpinnerNumberModel model1 = new SpinnerNumberModel(0,0,600,1);
        jtfPlazas = new JSpinner(model1);
        jlPlazas = new JLabel("Plazas");
        jpMenuA.add(jlPlazas);
        jpMenuA.add(jtfPlazas);

        SpinnerNumberModel model2 = new SpinnerNumberModel(0,0,65525,1);
        jlAltura = new JLabel("Altura del avion");
        jtfAltura = new JSpinner(model2);
        jpMenuA.add(jlAltura);
        jpMenuA.add(jtfAltura);

        this.add(jpMenuA, BorderLayout.NORTH);

        jpMenuA1 = new JPanel(new GridLayout(1, 2));

        jbAceptar = new JButton("Aceptar");
        jbEliminar = new JButton("Eliminar");

        jpMenuA1.add(jbAceptar);
        jpMenuA1.add(jbEliminar);

        this.add(jpMenuA1, BorderLayout.SOUTH);


        setSize(850, 800);
        setVisible(true);
        this.setBorder(BorderFactory.createTitledBorder("Agregar o eleminar aviones"));
    }

    public void cleanFields() {

        jtfAvion.setText("");

        jtfModeloA.setText("");


    }

    public void setController(AvionController controller) {
        jbAceptar.setActionCommand("Aceptar");
        jbAceptar.addActionListener(controller);
        jbEliminar.setActionCommand("Eliminar");
        jbEliminar.addActionListener(controller);
    }

    public JButton getJbAceptar() {
        return jbAceptar;
    }

    public void setJbAceptar(JButton jbAceptar) {
        this.jbAceptar = jbAceptar;
    }

    public JButton getJbEliminar() {
        return jbEliminar;
    }

    public void setJbEliminar(JButton jbEliminar) {
        this.jbEliminar = jbEliminar;
    }

    public JPanel getJpMenuA() {
        return jpMenuA;
    }

    public void setJpMenuA(JPanel jpMenuA) {
        this.jpMenuA = jpMenuA;
    }

    public JPanel getJpMenuA1() {
        return jpMenuA1;
    }

    public void setJpMenuA1(JPanel jpMenuA1) {
        this.jpMenuA1 = jpMenuA1;
    }

    public JTextField getJtfAvion() {
        return jtfAvion;
    }

    public void setJtfAvion(JTextField jtfAvion) {
        this.jtfAvion = jtfAvion;
    }

    public JLabel getJlAvion() {
        return jlAvion;
    }

    public void setJlAvion(JLabel jlAvion) {
        this.jlAvion = jlAvion;
    }

    public JTextField getJtfModeloA() {
        return jtfModeloA;
    }

    public void setJtfModeloA(JTextField jtfModeloA) {
        this.jtfModeloA = jtfModeloA;
    }

    public JLabel getJlModeloA() {
        return jlModeloA;
    }

    public void setJlModeloA(JLabel jlModeloA) {
        this.jlModeloA = jlModeloA;
    }

    public JSpinner getJtfPlazas() {
        return jtfPlazas;
    }

    public void setJtfPlazas(JSpinner jtfPlazas) {
        this.jtfPlazas = jtfPlazas;
    }

    public JLabel getJlPlazas() {
        return jlPlazas;
    }

    public void setJlPlazas(JLabel jlPlazas) {
        this.jlPlazas = jlPlazas;
    }

    public JLabel getJlAltura() {
        return jlAltura;
    }

    public void setJlAltura(JLabel jlAltura) {
        this.jlAltura = jlAltura;
    }

    public JSpinner getJtfAltura() {
        return jtfAltura;
    }

    public void setJtfAltura(JSpinner jtfAltura) {
        this.jtfAltura = jtfAltura;
    }
}


