package view;

import database.ConectorDB;
import model.Vuelo;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.util.LinkedList;

/**
 * Created by gabri mod Anton
 */
public class VentanaInformacionVuelo extends JPanel{

    private JPanel jpTabla;
    private DefaultTableModel defaultTableModel;

    public VentanaInformacionVuelo() {

        LinkedList<Vuelo> vuelos;
        ConectorDB conn = ConectorDB.getInstance();
        vuelos = conn.getVuelosDbDisponibles();

        jpTabla= new JPanel ();
        defaultTableModel = new DefaultTableModel();
        JTable table = new JTable(defaultTableModel);

        defaultTableModel.addColumn("Referencia");
        defaultTableModel.addColumn("Estado del vuelo");
        defaultTableModel.addColumn("Feccha de vuelo");
        defaultTableModel.addColumn("Duracion");
        defaultTableModel.addColumn("Retardo");
        for(Vuelo vuelo : vuelos){
            defaultTableModel.addRow(new Object[]{vuelo.getReferencia(), vuelo.getEstado(),
                    vuelo.getFechaSalida(), vuelo.getDuracion(), vuelo.getRetraso()});
        }

        table.setPreferredScrollableViewportSize(new Dimension(440, 200));

        //Creamos un JscrollPane y le agregamos la JTable
        JScrollPane scrollPane = new JScrollPane(table);

        //Agregamos el JScrollPane al contenedor
        jpTabla.add(scrollPane, BorderLayout.CENTER);
        //manejamos la salida

        this.add(jpTabla);

        setSize(500, 100);
        this.setVisible(true);
        this.setBorder(BorderFactory.createTitledBorder("Vuelos"));
    }

    public void actualizaTablaVuelos(){
        LinkedList<Vuelo> vuelos;
        ConectorDB conn = ConectorDB.getInstance();
        vuelos = conn.getVuelosDbDisponibles();

        defaultTableModel.setRowCount(0);
        defaultTableModel.setColumnCount(0);
        defaultTableModel.addColumn("Referencia");
        defaultTableModel.addColumn("Estado del vuelo");
        defaultTableModel.addColumn("Feccha de vuelo");
        defaultTableModel.addColumn("Duracion");
        defaultTableModel.addColumn("Retardo");
        for(Vuelo vuelo : vuelos){
            defaultTableModel.addRow(new Object[]{vuelo.getReferencia(), vuelo.getEstado(),
                    vuelo.getFechaSalida(), vuelo.getDuracion(), vuelo.getRetraso()});
        }
    }


}



