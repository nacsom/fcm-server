package view;

import controllers.VueloController;

import javax.swing.*;
import java.awt.*;
import java.text.DateFormat;
import java.util.Date;

/**
 * Created by gabri_
 */

public class VentanaVuelo extends JPanel {

    private JButton jbAceptar;
    private JButton jbEliminar;
    private JPanel jpTotal;

    private JPanel jpMenuV;
    private JPanel jpMenuV1;

    private JTextField jtfVuelo;
    private JLabel jlVuelo;

    private DateTimePicker dateTimePicker;
    private JLabel jlFecha;
    private Date jtfFecha;

    private JComboBox<String> jcbEstado; //combobox para estado
    private JLabel jlEstado;

    private JLabel jlDuracion;
    private JSpinner jtfDuracion;

    private JLabel jlRetardo;
    private JSpinner jtfRetardo;

    private JLabel jlRefeAvion;
    private JTextField jtfRefeAvion;

    private JLabel jlReferenciaO;
    private JTextField jtfReferenciaO;

    private JLabel jlReferenciaD;
    private JTextField jtfReferenciaD;

    private JLabel jlPincode;
    private JSpinner jtfPincode;

    private JPanel jpMenuV2;

    public VentanaVuelo (){


        jpMenuV= new JPanel(new GridLayout(10,2));

        jpMenuV2= new JPanel ();

        jlVuelo = new JLabel("Referencia");
        jtfVuelo = new JTextField();
        jpMenuV.add(jlVuelo);
        jpMenuV.add(jtfVuelo);

        // Jcombox para los datos del estado del vuelo

        jlEstado= new JLabel ("Estado del vuelo");
        jcbEstado = new JComboBox<>();
        jcbEstado.addItem("On-Time");
        //jcbEstado.addItem("Delayed");

       // jpMenuV.add(jlEstado);
        //jpMenuV.add(jcbEstado);


        jlFecha= new JLabel("fecha de vuelo");

        Date date = new Date();

        dateTimePicker = new DateTimePicker();
        dateTimePicker.setFormats( DateFormat.getDateTimeInstance( DateFormat.SHORT, DateFormat.MEDIUM ) );
        dateTimePicker.setTimeFormat( DateFormat.getTimeInstance( DateFormat.MEDIUM ) );

        dateTimePicker.setDate(date);
        jtfFecha = dateTimePicker.getDate();
        jpMenuV.add(jlFecha);
        jpMenuV.add(dateTimePicker);

        // jpMenuV.add(picker);

        SpinnerNumberModel model2 = new SpinnerNumberModel(0,0,1440,1);
        jlDuracion= new JLabel("Duracion");
        jpMenuV.add(jlDuracion);
        jtfDuracion= new JSpinner(model2);
        jpMenuV.add(jtfDuracion);

        //SpinnerNumberModel model1 = new SpinnerNumberModel(0,0,6000,1);
       // jlRetardo= new JLabel("Retardo");
       // jtfRetardo= new JSpinner(model1);
       // jpMenuV.add(jlRetardo);
       // jpMenuV.add(jtfRetardo);

        jlRefeAvion= new JLabel ("Referencia de avion");
        jtfRefeAvion= new JTextField();
        jpMenuV.add(jlRefeAvion);
        jpMenuV.add(jtfRefeAvion);

        jlReferenciaO= new JLabel("Referencia origen");
        jtfReferenciaO= new JTextField();
        jpMenuV.add(jlReferenciaO);
        jpMenuV.add(jtfReferenciaO);


        jlReferenciaD= new JLabel("Referencia destino");
        jtfReferenciaD= new JTextField();
        jpMenuV.add(jlReferenciaD);
        jpMenuV.add(jtfReferenciaD);

        SpinnerNumberModel model3= new SpinnerNumberModel(0,0,9999,1);
        jlPincode= new JLabel("Pincode");
        jtfPincode= new JSpinner(model3);
        jpMenuV.add(jlPincode);
        jpMenuV.add(jtfPincode);


        jpMenuV1= new JPanel ( new GridLayout(2,1));

        jbAceptar = new JButton("Aceptar");
        jbEliminar = new JButton("Eliminar");
        jpMenuV1.add(jbAceptar);
        jpMenuV1.add(jbEliminar);

        //jpMenuV2.add(jpMenuV);
        //jpMenuV2.add(jpMenuV1);

        this.add(jpMenuV);
        this.add(jpMenuV1);
        jpMenuV.setVisible(true);
        jpMenuV1.setVisible(true);

        this.setBorder(BorderFactory.createTitledBorder("Agregar o eleminar vuelos"));
    }



    public void setController(VueloController controller) {
        jbAceptar.setActionCommand("Aceptar");
        jbAceptar.addActionListener(controller);
        jbEliminar.setActionCommand("Eliminar");
        jbEliminar.addActionListener(controller);
    }


    public JLabel getJlPincode() {
        return jlPincode;
    }

    public void setJlPincode(JLabel jlPincode) {
        this.jlPincode = jlPincode;
    }

    public JSpinner getJtfPincode() {
        return jtfPincode;
    }

    public void setJtfPincode(JSpinner jtfPincode) {
        this.jtfPincode = jtfPincode;
    }

    public JButton getJbAceptar() {
        return jbAceptar;
    }

    public void setJbAceptar(JButton jbAceptar) {
        this.jbAceptar = jbAceptar;
    }

    public JButton getJbEliminar() {
        return jbEliminar;
    }

    public void setJbEliminar(JButton jbEliminar) {
        this.jbEliminar = jbEliminar;
    }

    public JPanel getJpTotal() {
        return jpTotal;
    }

    public void setJpTotal(JPanel jpTotal) {
        this.jpTotal = jpTotal;
    }

    public JPanel getJpMenuV() {
        return jpMenuV;
    }

    public void setJpMenuV(JPanel jpMenuV) {
        this.jpMenuV = jpMenuV;
    }

    public JPanel getJpMenuV1() {
        return jpMenuV1;
    }

    public void setJpMenuV1(JPanel jpMenuV1) {
        this.jpMenuV1 = jpMenuV1;
    }

    public JTextField getJtfVuelo() {
        return jtfVuelo;
    }

    public void setJtfVuelo(JTextField jtfVuelo) {
        this.jtfVuelo = jtfVuelo;
    }

    public JLabel getJlVuelo() {
        return jlVuelo;
    }

    public void setJlVuelo(JLabel jlVuelo) {
        this.jlVuelo = jlVuelo;
    }

    public JLabel getJlFecha() {
        return jlFecha;
    }

    public void setJlFecha(JLabel jlFecha) {
        this.jlFecha = jlFecha;
    }

    public Date getJtfFecha() {
        return jtfFecha;
    }

    public void setJtfFecha(Date jtfFecha) {
        this.jtfFecha = jtfFecha;
    }

    public JComboBox<String> getJcbEstado() {
        return jcbEstado;
    }

    public void setJcbEstado(JComboBox<String> jcbEstado) {
        this.jcbEstado = jcbEstado;
    }

    public JLabel getJlEstado() {
        return jlEstado;
    }

    public void setJlEstado(JLabel jlEstado) {
        this.jlEstado = jlEstado;
    }

    public JLabel getJlDuracion() {
        return jlDuracion;
    }

    public void setJlDuracion(JLabel jlDuracion) {
        this.jlDuracion = jlDuracion;
    }

    public JSpinner getJtfDuracion() {
        return jtfDuracion;
    }

    public void setJtfDuracion(JSpinner jtfDuracion) {
        this.jtfDuracion = jtfDuracion;
    }

    public JLabel getJlRetardo() {
        return jlRetardo;
    }

    public void setJlRetardo(JLabel jlRetardo) {
        this.jlRetardo = jlRetardo;
    }

    public JSpinner getJtfRetardo() {
        return jtfRetardo;
    }

    public void setJtfRetardo(JSpinner jtfRetardo) {
        this.jtfRetardo = jtfRetardo;
    }

    public JLabel getJlRefeAvion() {
        return jlRefeAvion;
    }

    public void setJlRefeAvion(JLabel jlRefeAvion) {
        this.jlRefeAvion = jlRefeAvion;
    }

    public JTextField getJtfRefeAvion() {
        return jtfRefeAvion;
    }

    public void setJtfRefeAvion(JTextField jtfRefeAvion) {
        this.jtfRefeAvion = jtfRefeAvion;
    }

    public JLabel getJlReferenciaO() {
        return jlReferenciaO;
    }

    public void setJlReferenciaO(JLabel jlReferenciaO) {
        this.jlReferenciaO = jlReferenciaO;
    }

    public JTextField getJtfReferenciaO() {
        return jtfReferenciaO;
    }

    public void setJtfReferenciaO(JTextField jtfReferenciaO) {
        this.jtfReferenciaO = jtfReferenciaO;
    }

    public JLabel getJlReferenciaD() {
        return jlReferenciaD;
    }

    public void setJlReferenciaD(JLabel jlReferenciaD) {
        this.jlReferenciaD = jlReferenciaD;
    }

    public JTextField getJtfReferenciaD() {
        return jtfReferenciaD;
    }

    public void setJtfReferenciaD(JTextField jtfReferenciaD) {
        this.jtfReferenciaD = jtfReferenciaD;
    }

    public DateTimePicker getDateTimePicker() {
        return dateTimePicker;
    }

    public void setDateTimePicker(DateTimePicker dateTimePicker) {
        this.dateTimePicker = dateTimePicker;
    }
}
