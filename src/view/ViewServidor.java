package view;

import controllers.MenuController;
import database.ConectorDB;

import javax.swing.*;
import java.awt.*;

public class ViewServidor  extends JFrame {


    private VentanaVuelosEnCurso ventanaVuelosEnCurso;
    private VentanaVuelo ventanaVuelo;
    private VentanaAvion ventanaAvion;
    private VentanaAeropuerto ventanaAeropuerto;
    private VentanaInformacionAeropuerto ventanaInformacionAeropuerto;
    private VentanaInformacionAvion ventanaInformacionAvion;
    private VentanaInformacionVuelo ventanaInformacionVuelo;
    private VentanaEstadistica ventanaEstadistica;

    private CardLayout layout;
    private CardLayout layout1;

    private JMenuBar jmbBarra;

    private JMenu jmMenuInformacion;
    private JMenuItem jmiInfoAvion;
    private JMenuItem jmiInfoVuelo;
    private JMenuItem jmiInfoAeropuerto;

    private JMenu jmMenuAgregarEliminar;
    private JMenuItem jmiAvion;
    private JMenuItem jmiVuelo;
    private JMenuItem jmiAeropuerto;

    private JPanel jpInferior;

    private JPanel jpVariado;
    private JPanel jpEstadisticas;

    private ConectorDB conn;


    public ViewServidor(VentanaAvion ventanaAvion, VentanaVuelo ventanaVuelo, VentanaVuelosEnCurso ventanaVuelosEnCurso, VentanaAeropuerto ventanaAeropuerto, VentanaInformacionAvion ventanaInformacionAvion,
                        VentanaInformacionVuelo ventanaInformacionVuelo, VentanaInformacionAeropuerto ventanaInformacionAeropuerto, VentanaEstadistica ventanaEstadistica) {

        this.ventanaVuelo = ventanaVuelo;
        this.ventanaAvion = ventanaAvion;
        this.ventanaVuelosEnCurso = ventanaVuelosEnCurso;
        this.ventanaInformacionAeropuerto = ventanaInformacionAeropuerto;
        this.ventanaAeropuerto = ventanaAeropuerto;
        this.ventanaInformacionAvion = ventanaInformacionAvion;
        this.ventanaInformacionVuelo = ventanaInformacionVuelo;
        this.ventanaEstadistica = ventanaEstadistica;
        conn = ConectorDB.getInstance();

//JPanel donde va la ventana Vuelos en curso, donde se vera el mapa mundo.
        JPanel jpImagen = new JPanel();
        layout1 = new CardLayout();
        jpImagen.setLayout(layout1);
        jpImagen.add(ventanaVuelosEnCurso);
        this.add(jpImagen, BorderLayout.CENTER);

// Panel inferior (toda la parte de abajo del JFrame.
        jpInferior = new JPanel(new GridLayout(1, 2));

        //Creacion de las diferentes opciones que tiene el ViewServer
        jmbBarra = new JMenuBar();
        jmMenuInformacion = new JMenu("Informacion");
        jmiInfoAeropuerto = new JMenuItem("Aeropuertos");
        jmiInfoAvion = new JMenuItem("Aviones");
        jmiInfoVuelo = new JMenuItem("vuelos");
        jmMenuInformacion.add(jmiInfoAvion);
        jmMenuInformacion.add(jmiInfoAeropuerto);
        jmMenuInformacion.add(jmiInfoVuelo);
        jmMenuAgregarEliminar = new JMenu("Agregar o eliminar");
        jmiAeropuerto = new JMenuItem("Aeropuerto");
        jmiAvion = new JMenuItem("Avion");
        jmiVuelo = new JMenuItem("vuelo");
        jmMenuAgregarEliminar.add(jmiAvion);
        jmMenuAgregarEliminar.add(jmiAeropuerto);
        jmMenuAgregarEliminar.add(jmiVuelo);
        jmbBarra.add(jmMenuInformacion);
        jmbBarra.add(jmMenuAgregarEliminar);
        jmbBarra.setVisible(true);

        //Panel donde se veran las diferentes opciones.

        jpVariado = new JPanel();
        layout = new CardLayout();
        jpVariado.setLayout(layout);

        jpVariado.add(ventanaInformacionVuelo, "Vuelos");
        jpVariado.add(ventanaInformacionAeropuerto, "Aeropuertos");
        jpVariado.add(ventanaInformacionAvion, "Aviones");
        jpVariado.add(ventanaAeropuerto, "Aeropuerto");
        jpVariado.add(ventanaVuelo, "Vuelo");
        jpVariado.add(ventanaAvion, "Avion");

        //Panel donde se unira la barra con las opciones y su informacion.
        JPanel jpBarraYopciones = new JPanel();
        jpBarraYopciones.add(jmbBarra);
        jpBarraYopciones.add(jpVariado, BorderLayout.NORTH);

        jpEstadisticas = new JPanel();
        jpEstadisticas.setBorder(BorderFactory.createTitledBorder("Estadisticas"));

        jpEstadisticas.setLayout(layout);
       jpEstadisticas.add(ventanaEstadistica);
;

        jpInferior.add(jpBarraYopciones);
        jpInferior.add(jpEstadisticas);

        this.add(jpInferior, BorderLayout.SOUTH);
        jpInferior.setVisible(true);

        //configuracion de ViewServer
        setTitle("SERVIDOR FCM");
        setSize(940, 710);
        this.setLocationRelativeTo(null);
        this.setResizable(false);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

    }


    public void registerController(MenuController mController) {

        // Las etiquetas para poder indentificar las diferentes opciones del menu
        jmiInfoAvion.setActionCommand("Aviones");
        jmiInfoAeropuerto.setActionCommand("Aeropuertos");
        jmiInfoVuelo.setActionCommand("Vuelos");

        jmiVuelo.setActionCommand("Vuelo");
        jmiAvion.setActionCommand("Avion");
        jmiAeropuerto.setActionCommand("Aeropuerto");


        // Registra el controlador de las diferentes opciones del menu
        jmiInfoAvion.addActionListener(mController);
        jmiInfoAeropuerto.addActionListener(mController);
        jmiInfoVuelo.addActionListener(mController);

        jmiVuelo.addActionListener(mController);
        jmiAvion.addActionListener(mController);
        jmiAeropuerto.addActionListener(mController);


    }


    public void mostraVentanaVuelo() {
        layout.show(jpVariado, "Vuelo");

    }

    public void mostraVentanaAvion() {
        layout.show(jpVariado, "Avion");
       ;
    }

    public void mostraVentanaAeropuerto() {
        layout.show(jpVariado, "Aeropuerto");

    }

    public void mostraVentanaInformacionAeropuerto() {
        layout.show(jpVariado, "Aeropuertos");

    }

    public void mostraVentanaInformacionVuelo() {
        layout.show(jpVariado, "Vuelos");

    }

    public void mostraVentanaInformacionAvion() {
        layout.show(jpVariado, "Aviones");

    }

}