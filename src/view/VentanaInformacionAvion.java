package view;

import database.ConectorDB;
import model.Avion;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.util.LinkedList;

/**
 * Created by gabri_mod Anton
 */
public class VentanaInformacionAvion extends JPanel{

    private JPanel jpTabla;
    private DefaultTableModel defaultTableModel;


    public VentanaInformacionAvion() {

        jpTabla= new JPanel ();

        LinkedList<Avion> aviones;
        ConectorDB conn = ConectorDB.getInstance();
        aviones = conn.getAvionesDisponibles();

        jpTabla= new JPanel ();
        defaultTableModel = new DefaultTableModel();
        JTable table = new JTable(defaultTableModel);

        defaultTableModel.addColumn("Referencia");
        defaultTableModel.addColumn("Modelo del avion");
        defaultTableModel.addColumn("Plazas");
        defaultTableModel.addColumn("Altura del avion");
        //array bidimencional de objetos con los datos de la tabla
        int i = 0;
        for(Avion avion : aviones){
            defaultTableModel.addRow(new Object[]{avion.getReferencia(), avion.getModelo(),
                    avion.getPlazas(), avion.getAltura()});
        }

        table.setPreferredScrollableViewportSize(new Dimension(440, 200));

        //Creamos un JscrollPane y le agregamos la JTable
        JScrollPane scrollPane = new JScrollPane(table);

        //Agregamos el JScrollPane al contenedor
        jpTabla.add(scrollPane, BorderLayout.CENTER);
        //manejamos la salida

        this.add(jpTabla);
        //setLayout(null);
        //setSize(1000, 100);
        this.setVisible(true);
        this.setBorder(BorderFactory.createTitledBorder("Aviones"));
    }

    public void actualizaTablaAviones(){
        LinkedList<Avion> aviones;
        ConectorDB conn = ConectorDB.getInstance();
        aviones = conn.getAvionesDisponibles();

        defaultTableModel.setRowCount(0);
        defaultTableModel.setColumnCount(0);
        defaultTableModel.addColumn("Referencia");
        defaultTableModel.addColumn("Modelo del avion");
        defaultTableModel.addColumn("Plazas");
        defaultTableModel.addColumn("Altura del avion");
        for(Avion avion : aviones){
            defaultTableModel.addRow(new Object[]{avion.getReferencia(), avion.getModelo(),
                    avion.getPlazas(), avion.getAltura()});
        }
    }

}


