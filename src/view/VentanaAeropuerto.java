package view;

import controllers.AeropuertoController;
import controllers.AvionController;

import javax.swing.*;
import java.awt.*;

/**
 * Created by gabri_000 on 11/04/2017.
 */
public class VentanaAeropuerto extends JPanel {

    private JButton jbAceptar;
    private JButton jbEliminar;

    private JLabel jlAeropuertoId;
    private JLabel jlAeropuertoNombre;
    private JTextField jtfAeropuertoNombre;
    private JTextField jtfAeropuertoId;
    private JLabel jlCoordenadaX;
    private JSpinner jtfCoordenadaX;
    private JLabel jlCoordenadaY;
    private JSpinner jtfCoordenadaY;
    private JPanel jpMenu;
    private JPanel jpMenu1;


    public VentanaAeropuerto() {
        jpMenu = new JPanel((new GridLayout(5, 2)));

        jlAeropuertoId = new JLabel("Referencia");
        jtfAeropuertoId = new JTextField();
        jpMenu.add(jlAeropuertoId);
        jpMenu.add(jtfAeropuertoId);

        jlAeropuertoNombre = new JLabel("Nombre del aeropuerto");
        jtfAeropuertoNombre = new JTextField();
        jpMenu.add(jlAeropuertoNombre);
        jpMenu.add(jtfAeropuertoNombre);


        SpinnerNumberModel model1 = new SpinnerNumberModel(0,0,65525,1);
        jlCoordenadaX = new JLabel("Coordenada X");
        jtfCoordenadaX = new JSpinner(model1);
        jpMenu.add(jlCoordenadaX);
        jpMenu.add(jtfCoordenadaX);


        SpinnerNumberModel model2 = new SpinnerNumberModel(0,0,65525,1);
        jlCoordenadaY = new JLabel("Coordenada Y");
        jtfCoordenadaY = new JSpinner(model2);
        jpMenu.add(jlCoordenadaY);
        jpMenu.add(jtfCoordenadaY);


        this.add(jpMenu, BorderLayout.NORTH);

        jpMenu1 = new JPanel((new GridLayout(2, 1)));

        jbAceptar = new JButton("Aceptar");
        jbEliminar = new JButton("Eliminar");
        jpMenu1.add(jbAceptar);
        jpMenu1.add(jbEliminar);

        this.add(jpMenu1, BorderLayout.SOUTH);

        //this.setSize(500, 500);

        this.setVisible(true);
        this.setBorder(BorderFactory.createTitledBorder("Agregar o eleminar aeropuertos"));
    }
    public void setController(AeropuertoController controller) {
        jbAceptar.setActionCommand("Aceptar");
        jbAceptar.addActionListener(controller);
        jbEliminar.setActionCommand("Eliminar");
        jbEliminar.addActionListener(controller);
    }

    public JButton getJbAceptar() {
        return jbAceptar;
    }

    public void setJbAceptar(JButton jbAceptar) {
        this.jbAceptar = jbAceptar;
    }

    public JButton getJbEliminar() {
        return jbEliminar;
    }

    public void setJbEliminar(JButton jbEliminar) {
        this.jbEliminar = jbEliminar;
    }

    public JLabel getJlAeropuertoId() {
        return jlAeropuertoId;
    }

    public void setJlAeropuertoId(JLabel jlAeropuertoId) {
        this.jlAeropuertoId = jlAeropuertoId;
    }

    public JLabel getJlAeropuertoNombre() {
        return jlAeropuertoNombre;
    }

    public void setJlAeropuertoNombre(JLabel jlAeropuertoNombre) {
        this.jlAeropuertoNombre = jlAeropuertoNombre;
    }

    public JTextField getJtfAeropuertoNombre() {
        return jtfAeropuertoNombre;
    }

    public void setJtfAeropuertoNombre(JTextField jtfAeropuertoNombre) {
        this.jtfAeropuertoNombre = jtfAeropuertoNombre;
    }

    public JTextField getJtfAeropuertoId() {
        return jtfAeropuertoId;
    }

    public void setJtfAeropuertoId(JTextField jtfAeropuertoId) {
        this.jtfAeropuertoId = jtfAeropuertoId;
    }

    public JLabel getJlCoordenadaX() {
        return jlCoordenadaX;
    }

    public void setJlCoordenadaX(JLabel jlCoordenadaX) {
        this.jlCoordenadaX = jlCoordenadaX;
    }

    public JSpinner getJtfCoordenadaX() {
        return jtfCoordenadaX;
    }

    public void setJtfCoordenadaX(JSpinner jtfCoordenadaX) {
        this.jtfCoordenadaX = jtfCoordenadaX;
    }

    public JLabel getJlCoordenadaY() {
        return jlCoordenadaY;
    }

    public void setJlCoordenadaY(JLabel jlCoordenadaY) {
        this.jlCoordenadaY = jlCoordenadaY;
    }

    public JSpinner getJtfCoordenadaY() {
        return jtfCoordenadaY;
    }

    public void setJtfCoordenadaY(JSpinner jtfCoordenadaY) {
        this.jtfCoordenadaY = jtfCoordenadaY;
    }

    public JPanel getJpMenu() {
        return jpMenu;
    }

    public void setJpMenu(JPanel jpMenu) {
        this.jpMenu = jpMenu;
    }

    public JPanel getJpMenu1() {
        return jpMenu1;
    }

    public void setJpMenu1(JPanel jpMenu1) {
        this.jpMenu1 = jpMenu1;
    }
}
