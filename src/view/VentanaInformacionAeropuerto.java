package view;

import database.ConectorDB;
import model.Aeropuerto;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.util.LinkedList;

public class VentanaInformacionAeropuerto extends JPanel {

    private JPanel jpTabla;
    private DefaultTableModel defaultTableModel;


    public VentanaInformacionAeropuerto() {

        LinkedList<Aeropuerto> aeropuertos;
        ConectorDB conn = ConectorDB.getInstance();
        aeropuertos = conn.getAeropuertosDisponibles();

        jpTabla= new JPanel ();
        defaultTableModel = new DefaultTableModel();
        JTable table = new JTable(defaultTableModel);

        defaultTableModel.addColumn("Referencia");
        defaultTableModel.addColumn("Nombre del aeropuerto");
        defaultTableModel.addColumn("Coordenada X");
        defaultTableModel.addColumn("Coordenada Y");
        int i = 0;
        for(Aeropuerto aeropuerto : aeropuertos){
            defaultTableModel.addRow(new Object[]{aeropuerto.getReferencia(), aeropuerto.getNom(),
                    aeropuerto.getCoordx(), aeropuerto.getCoordy()});
        }

        table.setPreferredScrollableViewportSize(new Dimension(440, 200));


        //Creamos un JscrollPane y le agregamos la JTable
        JScrollPane scrollPane = new JScrollPane(table);

        //Agregamos el JScrollPane al contenedor
        jpTabla.add(scrollPane, BorderLayout.CENTER);
        //manejamos la salida
        this.setBorder(BorderFactory.createTitledBorder("Aeropuertos"));
        this.add(jpTabla);

        this.setVisible(true);
    }

    public void actualizaTablaAeropuertos(){
        LinkedList<Aeropuerto> aeropuertos = new LinkedList<>();
        ConectorDB conn = ConectorDB.getInstance();
        aeropuertos = conn.getAeropuertosDisponibles();

        defaultTableModel.setRowCount(0);
        defaultTableModel.setColumnCount(0);
        defaultTableModel.addColumn("Referencia");
        defaultTableModel.addColumn("Nombre del aeropuerto");
        defaultTableModel.addColumn("Coordenada X");
        defaultTableModel.addColumn("Coordenada Y");
        //array bidimencional de objetos con los datos de la tabla
        for(Aeropuerto aeropuerto : aeropuertos){
            defaultTableModel.addRow(new Object[]{aeropuerto.getReferencia(), aeropuerto.getNom(),
                    aeropuerto.getCoordx(), aeropuerto.getCoordy()});
        }

        //array de String's con los títulos de las columnas
        String[] columnNames = {"Referencia", "Nombre del aeropuerto", "Coordenada X",
                "Coordenada Y"};

    }

}

