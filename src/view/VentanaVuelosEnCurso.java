package view;

import database.ConectorDB;
import model.Aeropuerto;
import model.Vuelo;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.LinkedList;

/**
 * Created by Gabriella
 */
/*Per al càlcul del temps de vol cal fer:    T = (5*DIST)/402 on DIST és la distància en coordenades [0-360]longitud i [0-180]latitud.
 */

public class VentanaVuelosEnCurso extends JPanel implements  Runnable{
    private ConectorDB conn;
    private ImageIcon imagenFondo;
    private Dimension tamanio;
    private LinkedList<Aeropuerto> aeropuertos;
    private LinkedList<Vuelo> vuelos;

    private Thread animator;
    private int x=0, y=0, corre = 0;
    private double m = 0;
    private double DELAY = 700; //Velocitat de recorrer tota la pantalla en 5min
    private final int HEIGHT = 390;
    private final int WIDTH = 900;

    final static float dash1[] = {10.0f};
    final static BasicStroke dashed = new BasicStroke(5, BasicStroke.CAP_BUTT, BasicStroke.JOIN_MITER, 10.0f, dash1, 0.0f);
    private BufferedImage imagenAvion;

    private LinkedList<LinkedList<Integer>> vuelosCoordX;
    private LinkedList<LinkedList<Integer>> vuelosCoordY;
    private LinkedList<Double> vuelosPendienteRecta;
    private LinkedList<Integer> xVuelos;
    private LinkedList<Integer> correVuelos;
    private LinkedList<Double> distanciaVuelos;
    private LinkedList<Integer> duracionVuelos;
    private LinkedList<String> fechaSalidaVuelos;
    private LinkedList<Boolean> flyingVuelos;
    private LinkedList<String> estadoVuelos;
    private LinkedList<Boolean> startItVuelos;
    private LinkedList<Integer> loginFlagVuelos;

    private LinkedList<LinkedList<Integer>> vuelosCoordXaux;
    private LinkedList<LinkedList<Integer>> vuelosCoordYaux;
    private LinkedList<Double> vuelosPendienteRectaaux;
    private LinkedList<Integer> xVuelosaux;
    private LinkedList<Integer> correVuelosaux;
    private LinkedList<Double> distanciaVuelosaux;
    private LinkedList<Integer> duracionVuelosaux;
    private LinkedList<String> fechaSalidaVuelosaux;
    private LinkedList<Boolean> flyingVuelosaux;
    private LinkedList<String> estadoVuelosaux;
    private LinkedList<Boolean> startItVuelosaux;
    private LinkedList<Integer> loginFlagVuelosaux;

    private int counter = 0, vuelosFinalizados = 0;

    public VentanaVuelosEnCurso(){
        try {
            imagenFondo= new ImageIcon(getClass().getResource("/imagen/mundoMapa.png"));
            imagenAvion = ImageIO.read(new File("src/imagen/avion.png"));
        } catch (IOException e) {
            e.printStackTrace();
        }

        conn = ConectorDB.getInstance();
        this.aeropuertos = conn.getAeropuertosDisponibles();
        this.vuelos = conn.getVuelosDbDisponibles();    //Agafem tots els aeroports disponibles i també els vols
        this.vuelosCoordX = new LinkedList();
        this.vuelosCoordY = new LinkedList();
        this.vuelosPendienteRecta = new LinkedList();
        this.xVuelos = new LinkedList();
        this.correVuelos = new LinkedList();
        this.distanciaVuelos = new LinkedList();
        this.duracionVuelos = new LinkedList();
        this.fechaSalidaVuelos = new LinkedList();
        this.flyingVuelos = new LinkedList();
        this.estadoVuelos = new LinkedList();
        this.startItVuelos = new LinkedList();
        this.loginFlagVuelos = new LinkedList();

        this.vuelosCoordXaux = new LinkedList();
        this.vuelosCoordYaux = new LinkedList();
        this.vuelosPendienteRectaaux = new LinkedList();
        this.xVuelosaux = new LinkedList();
        this.correVuelosaux = new LinkedList();
        this.distanciaVuelosaux = new LinkedList();
        this.duracionVuelosaux = new LinkedList();
        this.fechaSalidaVuelosaux = new LinkedList();
        this.flyingVuelosaux = new LinkedList();
        this.estadoVuelosaux = new LinkedList();
        this.startItVuelosaux = new LinkedList();
        this.loginFlagVuelosaux = new LinkedList();

        if(generateListOfVuelos()) {    //Control per si no hi ha vols!
            this.setVisible(true);
            x = vuelosCoordX.get(0).get(0);
            y = vuelosCoordY.get(0).get(0);
            m = ((float) Math.abs(vuelosCoordY.get(0).get(1) - y)) / Math.abs(vuelosCoordX.get(0).get(1) - x);   //PENDENT PRIMER VOL
        }
    }

    @Override
    public void addNotify() {
        super.addNotify();
        if(vuelos.size() != 0) {
            animator = new Thread(this);
            animator.start();
        }
    }

    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        Graphics2D g2d = (Graphics2D)g;
        tamanio = getSize();
        g2d.drawImage(imagenFondo.getImage(), 0, 0, tamanio.width, tamanio.height, null);
        paintLineasVuelo(g2d);
        paintAirports(g2d);
        paintMovimientoVuelos(g2d);
        g.dispose();
    }
    public void cycle() {

    }

    public void paintMovimientoVuelos(Graphics2D g2d){
        int numberOfVuelos = vuelosCoordX.size();
        for(int numVuelo = 0; numVuelo < numberOfVuelos; numVuelo++){
            Calendar cal = Calendar.getInstance();
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            if(fechaSalidaVuelos.get(numVuelo).equals(sdf.format(cal.getTime()))){
                startItVuelos.set(numVuelo, true); //Indiquem que ha arribat l'hora de sortir
            }
            if(startItVuelos.get(numVuelo)) g2d.drawImage(imagenAvion, vuelosCoordX.get(numVuelo).get(0), vuelosCoordY.get(numVuelo).get(0), 40, 40, null);
            //Si ha arribat l'hora, pinta el vol al seu origen però no es mourà fins que estigui a tram creuer
            if((startItVuelos.get(numVuelo)&&estadoVuelos.get(numVuelo).equals("ON-TIME")&&loginFlagVuelos.get(numVuelo) == 1) ||
                    (loginFlagVuelos.get(numVuelo) == 0)&&startItVuelos.get(numVuelo)) {
                flyingVuelos.set(numVuelo, true); //Si ja ha sortit (hora) i està en tram de creuer, aleshores SURT
                startItVuelos.set(numVuelo, false);
            }
            //Així aconseguim que l'avió no es mogui fins que no ha pujat tota l'altura de crucero...
            if(flyingVuelos.get(numVuelo)) {    //Si l'avió està FlYING, aleshores fes que es mogui, sinó --> ESTATIC
                if (!xVuelos.get(numVuelo).equals(vuelosCoordX.get(numVuelo).get(1))) {
                    if (vuelosCoordX.get(numVuelo).get(0) > vuelosCoordX.get(numVuelo).get(1)) {
                        x = xVuelos.set(numVuelo, xVuelos.get(numVuelo) - 1);
                    } else {
                        x = xVuelos.set(numVuelo, xVuelos.get(numVuelo) + 1);
                    }
                    y = Integer.valueOf((int) (vuelosCoordY.get(numVuelo).get(0) - vuelosPendienteRecta.get(numVuelo) * correVuelos.get(numVuelo)));
                    correVuelos.set(numVuelo, correVuelos.get(numVuelo) + 1);
                    g2d.drawImage(imagenAvion, x, y, 40, 40, null);
                } else {  //VOL DIR QUE HA ARRIBAT AL SEU DESTÍ
                    g2d.drawImage(imagenAvion, vuelosCoordX.get(numVuelo).get(1), vuelosCoordY.get(numVuelo).get(1), 40, 40, null);
                    if(estadoVuelos.get(numVuelo).equals("LANDED") || loginFlagVuelos.get(numVuelo) == 0) {
                        vuelosFinalizados++;
                        estadoVuelos.set(numVuelo, "ON-GATE"); //Canviem l'estat perquè no entri més cops
                        flyingVuelos.set(numVuelo, false); //Indiquem que ja no esta volant.
                    }
                }
            }else{
                //g2d.drawImage(imagenAvion, vuelosCoordX.get(numVuelo).get(0), vuelosCoordY.get(numVuelo).get(0), 40, 40, null);
            }   //SI EL VOL NO ESTA CORRENT, EL PINTEM POSICIONAT EN EL SEU ORIGEN
        }
    }

    public void paintLineasVuelo(Graphics2D g2d){
        int numberOfVuelos = vuelosCoordX.size();
        for(int numVuelo = 0; numVuelo < numberOfVuelos; numVuelo++) {
            if(startItVuelos.get(numVuelo) || flyingVuelos.get(numVuelo)) {
                g2d.setStroke(dashed);
                g2d.setColor(Color.WHITE.brighter());
                g2d.drawLine(vuelosCoordX.get(numVuelo).get(0), vuelosCoordY.get(numVuelo).get(0), vuelosCoordX.get(numVuelo).get(1), vuelosCoordY.get(numVuelo).get(1));
            }
        }
    }

    public void paintAirports(Graphics2D g2d){  //POSICIONEM ELS AEROPORTS AL MAPA
        for(int i = 0; i < aeropuertos.size(); i++) {
            int coordy = Integer.valueOf((aeropuertos.get(i).getCoordy() * 390) / 180); //LATITUD
            int coordx = Integer.valueOf((aeropuertos.get(i).getCoordx() * 900) / 360); //LONGITUD
            g2d.setColor(Color.MAGENTA.brighter());
            g2d.fillOval(coordx, coordy, 15, 15);
            g2d.setColor(Color.WHITE.brighter());
        }
    }

    public boolean generateListOfVuelos() {
        int numOrigen = 0, numDestino = 0, index = 0;
        int numberOfVuelos = vuelos.size();
        boolean isVoid = false, hiHaAlgun = false;
        for(int numVuelo = 0; numVuelo < numberOfVuelos; numVuelo++) {
            boolean hiHaOrigen = false;
            boolean hihaDesti = false;
            for (int i = 0; i < aeropuertos.size(); i++){
                if (vuelos.get(numVuelo).getReferenciaOrigen().equals(aeropuertos.get(i).getReferencia())) {
                    numOrigen = i;
                    hiHaOrigen = true;
                }   //Caldria posar condició de si el vol està corrent en aquest moment o no. //
                if (vuelos.get(numVuelo).getGetReferenciaDestina().equals(aeropuertos.get(i).getReferencia())) {
                    numDestino = i;     //Agafem l'aeroport origen i destí de cada vol
                    hihaDesti = true;
                }
            }
            if(hiHaOrigen&&hihaDesti) {
                hiHaAlgun = true;
                int coordyOrigen = Integer.valueOf((aeropuertos.get(numOrigen).getCoordy() * 390) / 180); //LATITUD
                int coordxOrigen = Integer.valueOf((aeropuertos.get(numOrigen).getCoordx() * 900) / 360); //LONGITUD
                int coordyDestino = Integer.valueOf((aeropuertos.get(numDestino).getCoordy() * 390) / 180); //LATITUD
                int coordxDestino = Integer.valueOf((aeropuertos.get(numDestino).getCoordx() * 900) / 360); //LONGITUD
                vuelosCoordX.add(new LinkedList());
                vuelosCoordY.add(new LinkedList());
                vuelosCoordX.get(index).add(coordxOrigen);
                vuelosCoordX.get(index).add(coordxDestino);
                vuelosCoordY.get(index).add(coordyOrigen);
                vuelosCoordY.get(index).add(coordyDestino);

                int coordX0 = vuelosCoordX.get(index).get(0);
                int coordY0 = vuelosCoordY.get(index).get(0);
                int coordX1 = vuelosCoordX.get(index).get(1);
                int coordY1 = vuelosCoordY.get(index).get(1);
                double pendiente = ((float)Math.abs(coordY1 - coordY0))/Math.abs(coordX1 - coordX0);;   //PENDENT RECTA VOL
                int xVuelo = vuelosCoordX.get(index).get(0);
                int corre = 0;
                double distancia = Math.hypot(Math.abs(coordX0 - coordX1), Math.abs(coordY0 - coordY1));
                int duracion = vuelos.get(index).getDuracion();

                if(coordY0 < coordY1) pendiente*= -1;  //Si Origen < Destí , pendent negatiu
                vuelosPendienteRecta.add(pendiente);
                xVuelos.add(xVuelo);
                distanciaVuelos.add(distancia);
                duracionVuelos.add(duracion);
                correVuelos.add(corre);
                this.fechaSalidaVuelos.add(vuelos.get(numVuelo).getFechaSalida());
                estadoVuelos.add(vuelos.get(numVuelo).getEstado());
                flyingVuelos.add(false);    //Cap ha sortit encara..
                startItVuelos.add(false);
                loginFlagVuelos.add(vuelos.get(index).getLoginFlag());
                index++;
                hiHaOrigen = hihaDesti = false;
            }
        }
        if(hiHaAlgun) return true;
        return false;
    }

    @Override
    public void run() {
        long beforeTime, timeDiff, sleep;

        beforeTime = System.currentTimeMillis();
        while (true) {

            cycle();
            repaint();

            timeDiff = System.currentTimeMillis() - beforeTime;
            sleep = (long) (DELAY - timeDiff);

            if (sleep < 0)
                sleep = 2;
            try {
                Thread.sleep(sleep);
                counter++;
                if(counter == 1){
                    generateListOfVuelos2();
                    counter = 0;
                }
            } catch (InterruptedException e) {
                System.out.println("interrupted");
            }

            beforeTime = System.currentTimeMillis();
        }
    }

    public void generateListOfVuelos2() {
        int numOrigen = 0, numDestino = 0, index = 0;
        this.aeropuertos = conn.getAeropuertosDisponibles();
        this.vuelos = conn.getVuelosDbDisponibles();    //Agafem tots els aeroports disponibles i també els vols
        int numberOfVuelos = vuelos.size();

        this.vuelosCoordXaux.clear();
        this.vuelosCoordYaux.clear();
        this.vuelosPendienteRectaaux.clear();
        this.xVuelosaux.clear();
        this.correVuelosaux.clear();
        this.distanciaVuelosaux.clear();
        this.duracionVuelosaux.clear();
        this.fechaSalidaVuelosaux.clear();
        this.flyingVuelosaux.clear();
        this.estadoVuelosaux.clear();
        this.startItVuelosaux.clear();
        this.loginFlagVuelosaux.clear();

        for(int numVuelo = 0; numVuelo < numberOfVuelos; numVuelo++) {
            boolean hiHaOrigen = false;
            boolean hihaDesti = false;
            for (int i = 0; i < aeropuertos.size(); i++){
                if (vuelos.get(numVuelo).getReferenciaOrigen().equals(aeropuertos.get(i).getReferencia())) {
                    numOrigen = i;
                    hiHaOrigen = true;
                }   //Caldria posar condició de si el vol està corrent en aquest moment o no. //
                if (vuelos.get(numVuelo).getGetReferenciaDestina().equals(aeropuertos.get(i).getReferencia())) {
                    numDestino = i;     //Agafem l'aeroport origen i destí de cada vol
                    hihaDesti = true;
                }
            }
            if(hiHaOrigen&&hihaDesti) {
                int coordyOrigen = Integer.valueOf((aeropuertos.get(numOrigen).getCoordy() * 390) / 180); //LATITUD
                int coordxOrigen = Integer.valueOf((aeropuertos.get(numOrigen).getCoordx() * 900) / 360); //LONGITUD
                int coordyDestino = Integer.valueOf((aeropuertos.get(numDestino).getCoordy() * 390) / 180); //LATITUD
                int coordxDestino = Integer.valueOf((aeropuertos.get(numDestino).getCoordx() * 900) / 360); //LONGITUD
                vuelosCoordXaux.add(new LinkedList());
                vuelosCoordYaux.add(new LinkedList());
                vuelosCoordXaux.get(index).add(coordxOrigen);
                vuelosCoordXaux.get(index).add(coordxDestino);
                vuelosCoordYaux.get(index).add(coordyOrigen);
                vuelosCoordYaux.get(index).add(coordyDestino);

                int coordX0 = vuelosCoordXaux.get(index).get(0);
                int coordY0 = vuelosCoordYaux.get(index).get(0);
                int coordX1 = vuelosCoordXaux.get(index).get(1);
                int coordY1 = vuelosCoordYaux.get(index).get(1);
                double pendiente = ((float)Math.abs(coordY1 - coordY0))/Math.abs(coordX1 - coordX0);;   //PENDENT RECTA VOL
                int xVuelo = vuelosCoordXaux.get(index).get(0);
                int corre = 0;
                double distancia = Math.hypot(Math.abs(coordX0 - coordX1), Math.abs(coordY0 - coordY1));
                int duracion = vuelos.get(index).getDuracion();

                if(coordY0 < coordY1) pendiente*= -1;  //Si Origen < Destí , pendent negatiu
                vuelosPendienteRectaaux.add(pendiente);
                xVuelosaux.add(xVuelo);
                distanciaVuelosaux.add(distancia);
                duracionVuelosaux.add(duracion);
                correVuelosaux.add(corre);
                this.fechaSalidaVuelosaux.add(vuelos.get(numVuelo).getFechaSalida());
                //System.out.println(vuelos.get(numVuelo).getFechaSalida());  //NO FUNCIONA PERO SUPONEMOS QUE DEVUELVE BIEN LA FECHA (CON HORA)
                this.estadoVuelosaux.add(vuelos.get(numVuelo).getEstado());
                flyingVuelosaux.add(false);    //Cap ha sortit encara..
                startItVuelosaux.add(false);
                loginFlagVuelosaux.add(vuelos.get(numVuelo).getLoginFlag());

                index++;
                hiHaOrigen = hihaDesti = false;
            }
        }
        int sizeAux = vuelosCoordXaux.size();
        int sizeNormal = vuelosCoordX.size();
        if(sizeAux > sizeNormal) {   //Hi ha nous vols..
            System.out.println("NEW");

            this.vuelosCoordX.add(vuelosCoordXaux.getLast());
            this.vuelosCoordY.add(vuelosCoordYaux.getLast());
            this.vuelosPendienteRecta.add(vuelosPendienteRectaaux.getLast());
            this.xVuelos.add(xVuelosaux.getLast());
            this.correVuelos.add(correVuelosaux.getLast());
            this.distanciaVuelos.add(distanciaVuelosaux.getLast());
            this.duracionVuelos.add(duracionVuelosaux.getLast());
            this.flyingVuelos.add(flyingVuelosaux.getLast());
            this.startItVuelos.add(startItVuelosaux.getLast());

            this.fechaSalidaVuelos = this.fechaSalidaVuelosaux;
        }
            this.estadoVuelos = this.estadoVuelosaux;               //Actualitzem estats i loginFlags
            this.loginFlagVuelos = this.loginFlagVuelosaux;

    }

    public int getVuelosEnCurso(){
        int size = flyingVuelos.size(), counter = 0;
        for(int i = 0; i < size; i++){
            if(flyingVuelos.get(i) || startItVuelos.get(i)) counter ++;
        }
        return  counter;    //Retorna quants vols flying = true hi ha en  aquest moment.
    }

    public int getVuelosFinalizados() {
        return vuelosFinalizados;
    }
}



