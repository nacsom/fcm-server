-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 18-05-2017 a las 10:00:30
-- Versión del servidor: 10.1.21-MariaDB
-- Versión de PHP: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `fcm`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `aeropuertos`
--

CREATE TABLE `aeropuertos` (
  `referencia` varchar(50) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `nom` varchar(50) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `coordx` int(11) DEFAULT NULL,
  `coordy` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Tabla con los aeropuertos' ROW_FORMAT=COMPACT;

--
-- Volcado de datos para la tabla `aeropuertos`
--

INSERT INTO `aeropuertos` (`referencia`, `nom`, `coordx`, `coordy`) VALUES
('BCN', 'Barcelona', 2, 3),
('SCQ', 'Santiago', 22, 99);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `aeropuertovuelos`
--

CREATE TABLE `aeropuertovuelos` (
  `id` int(11) NOT NULL,
  `referenciaAeropuerto` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `referenciaVuelo` varchar(50) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci ROW_FORMAT=COMPACT;

--
-- Volcado de datos para la tabla `aeropuertovuelos`
--

INSERT INTO `aeropuertovuelos` (`id`, `referenciaAeropuerto`, `referenciaVuelo`) VALUES
(1, '', 'Mad-Bar'),
(2, '', 'Mad-Bar'),
(3, '', 'MDC'),
(4, '', 'masd'),
(5, '', 'BCNSCQ'),
(6, '', 'MDCA'),
(7, '', 'asd'),
(8, 'BCN', 'asd'),
(9, 'cc', 'asd'),
(10, 'BCBN', 'aasd'),
(11, 'BCN', 'asd'),
(12, 'asd', 'asd'),
(13, 'asd', 'asd'),
(14, 'BCN', '12234'),
(15, 'BCN', '456576'),
(16, 'BCN', '65454'),
(17, 'SCQ', '13521'),
(18, 'SCQ', '13521'),
(19, 'SCQ', '1111'),
(20, 'SCQ', '5412345'),
(21, 'SCQ', '5412342'),
(22, 'SCQ', '1412342'),
(23, 'BCN', 'vuelo1'),
(24, 'BCN', '235'),
(25, 'BCN', 'vuelo2'),
(26, 'BCN', 'vuelo3'),
(27, 'BCN', 'vuelo5'),
(28, 'BCN', 'vuelo11'),
(29, 'SCQ', 'vuelo11'),
(30, 'SCQ', 'vuelo11'),
(31, 'BCN', 'vuelo1'),
(32, 'BCN', 'vuelo11'),
(33, 'BCN', 'vuelo11'),
(34, 'BCN', 'vuelo11'),
(35, 'BCN', 'vuelo11'),
(36, 'SCQ', 'vuelo1');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `aviones`
--

CREATE TABLE `aviones` (
  `referencia` varchar(50) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `modelo` varchar(50) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `plazas` int(11) NOT NULL,
  `altura` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Tabla con los aviones';

--
-- Volcado de datos para la tabla `aviones`
--

INSERT INTO `aviones` (`referencia`, `modelo`, `plazas`, `altura`) VALUES
('1234', '1234', 1234, 1234),
('8436BA', '747-Boing', 252, 0),
('avion1', 'modelo1', 19, 20);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `avionvuelos`
--

CREATE TABLE `avionvuelos` (
  `id` int(11) NOT NULL,
  `referenciaAvion` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `referenciaVuelo` varchar(50) COLLATE utf8_spanish_ci DEFAULT NULL,
  `fechaSalida` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci ROW_FORMAT=COMPACT;

--
-- Volcado de datos para la tabla `avionvuelos`
--

INSERT INTO `avionvuelos` (`id`, `referenciaAvion`, `referenciaVuelo`, `fechaSalida`) VALUES
(19, '1234', '5412345', '2017-05-25 22:49:00'),
(20, '1234', '5412342', '2017-05-26 22:49:00'),
(21, '1234', '1412342', '2017-05-28 22:49:00'),
(22, '1234', '1111', '2017-05-09 00:00:00'),
(23, 'avion1', 'vuelo1', '2017-05-19 13:19:00'),
(24, 'avion1', '235', '2017-05-16 11:32:09'),
(25, 'avion1', 'vuelo2', '2017-05-17 19:52:00'),
(26, 'avion1', 'vuelo3', '2017-05-18 19:52:00'),
(27, '1234', 'vuelo5', '2017-05-17 20:45:08'),
(28, '1111', 'vuelo11', '2017-05-18 08:52:22'),
(29, '1111', 'vuelo11', '2017-05-27 08:55:00'),
(30, '1234', 'vuelo11', '2017-05-18 08:56:01'),
(31, '1234', 'vuelo1', '2017-05-22 08:58:00'),
(32, '1234', 'vuelo11', '2017-05-31 08:58:00'),
(33, '1234', 'vuelo11', '2017-05-01 08:58:00'),
(34, '1234', 'vuelo11', '2017-05-02 09:09:00'),
(35, '1234', 'vuelo11', '2017-05-04 09:12:00'),
(36, '1234', 'vuelo1', '2017-05-05 09:42:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `billetes`
--

CREATE TABLE `billetes` (
  `referencia` varchar(50) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `referenciaUsuario` varchar(50) CHARACTER SET utf8 COLLATE utf8_spanish_ci DEFAULT NULL,
  `referenciaVuelo` varchar(50) CHARACTER SET utf8 COLLATE utf8_spanish_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Tabla con los billetes';

--
-- Volcado de datos para la tabla `billetes`
--

INSERT INTO `billetes` (`referencia`, `referenciaUsuario`, `referenciaVuelo`) VALUES
('1', 'anton', '1111'),
('2', 'luis', '1111');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuariobilletes`
--

CREATE TABLE `usuariobilletes` (
  `id` int(11) NOT NULL,
  `referenciaUsuario` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `referenciaBillete` varchar(50) COLLATE utf8_spanish_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci ROW_FORMAT=COMPACT;

--
-- Volcado de datos para la tabla `usuariobilletes`
--

INSERT INTO `usuariobilletes` (`id`, `referenciaUsuario`, `referenciaBillete`) VALUES
(1, '123', '4321'),
(2, 'anton', '1'),
(3, 'luis', '2');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `username` varchar(50) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `password` varchar(50) CHARACTER SET utf8 COLLATE utf8_spanish_ci DEFAULT NULL,
  `nom` varchar(50) CHARACTER SET utf8 COLLATE utf8_spanish_ci DEFAULT NULL,
  `apellido` varchar(50) CHARACTER SET utf8 COLLATE utf8_spanish_ci DEFAULT NULL,
  `email` text CHARACTER SET utf8 COLLATE utf8_spanish_ci
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Tabla con los usuarios';

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`username`, `password`, `nom`, `apellido`, `email`) VALUES
('anton', 'anton', 'anton', 'anton', 'anton123'),
('luis', 'luis', 'luis', 'luis', 'luis');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `vuelos`
--

CREATE TABLE `vuelos` (
  `indice` int(11) NOT NULL,
  `referencia` varchar(50) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `pincode` int(4) DEFAULT NULL,
  `fechaSalida` datetime DEFAULT NULL,
  `duracion` int(11) NOT NULL,
  `retraso` int(11) NOT NULL,
  `estado` varchar(50) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `referenciaAvion` varchar(50) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `referenciaOrigen` varchar(50) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `referenciaDestino` varchar(50) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `plazasDisponibles` int(11) DEFAULT NULL,
  `modeloAvion` varchar(50) CHARACTER SET utf8 COLLATE utf8_spanish_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci COMMENT='Tabla con los vuelos' ROW_FORMAT=COMPACT;

--
-- Volcado de datos para la tabla `vuelos`
--

INSERT INTO `vuelos` (`indice`, `referencia`, `pincode`, `fechaSalida`, `duracion`, `retraso`, `estado`, `referenciaAvion`, `referenciaOrigen`, `referenciaDestino`, `plazasDisponibles`, `modeloAvion`) VALUES
(1, '1111', 1234, '2017-05-09 22:23:47', 123, 132, 'On-Time', '1234', 'SCQ', 'BCN', 123, ''),
(2, '1412342', 1230, '2017-05-28 22:49:00', 123, 123, 'Delay', '1234', 'SCQ', 'BCN', 2, ''),
(3, '235', 1110, '2017-05-17 20:56:00', 13, 1, 'On-Time', 'avion1', 'BCN', 'SCQ', 19, 'modelo1'),
(4, '5412342', 1230, '2017-05-17 20:56:00', 123, 123, 'Delay', '1234', 'SCQ', 'BCN', 1, ''),
(5, '5412345', 1230, '2017-05-17 20:56:00', 123, 123, 'Delay', '1234', 'SCQ', 'BCN', 2, ''),
(6, 'vuelo1', 4321, '2017-05-17 20:56:00', 1, 0, 'LANDED', 'avion1', 'BCN', 'SCQ', 19, 'modelo1'),
(7, 'vuelo2', 1111, '2017-05-17 20:56:00', 12, 12, 'On-Time', 'avion1', 'BCN', 'SCQ', 19, 'modelo1'),
(8, 'vuelo3', 1111, '2017-05-17 20:56:00', 12, 12, 'On-Time', 'avion1', 'BCN', 'SCQ', 19, 'modelo1'),
(9, 'vuelo5', 1234, '2017-05-17 20:45:08', 123, 123, 'On-Time', '1234', 'BCN', 'SCQ', 1234, '1234'),
(11, 'vuelo11', 1234, '2017-05-31 08:58:00', 123, 123, 'On-Time', '1234', 'BCN', 'SCQ', 1234, '1234');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `aeropuertos`
--
ALTER TABLE `aeropuertos`
  ADD PRIMARY KEY (`referencia`),
  ADD UNIQUE KEY `referencia` (`referencia`);

--
-- Indices de la tabla `aeropuertovuelos`
--
ALTER TABLE `aeropuertovuelos`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id_2` (`id`),
  ADD KEY `id` (`id`);

--
-- Indices de la tabla `aviones`
--
ALTER TABLE `aviones`
  ADD PRIMARY KEY (`referencia`),
  ADD UNIQUE KEY `referencia` (`referencia`);

--
-- Indices de la tabla `avionvuelos`
--
ALTER TABLE `avionvuelos`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id_2` (`id`),
  ADD KEY `id` (`id`);

--
-- Indices de la tabla `billetes`
--
ALTER TABLE `billetes`
  ADD PRIMARY KEY (`referencia`),
  ADD UNIQUE KEY `referencia` (`referencia`);

--
-- Indices de la tabla `usuariobilletes`
--
ALTER TABLE `usuariobilletes`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id_2` (`id`),
  ADD KEY `id` (`id`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`username`),
  ADD UNIQUE KEY `username` (`username`);

--
-- Indices de la tabla `vuelos`
--
ALTER TABLE `vuelos`
  ADD PRIMARY KEY (`indice`),
  ADD UNIQUE KEY `referencia` (`referencia`),
  ADD UNIQUE KEY `indice_2` (`indice`),
  ADD KEY `INDEX` (`indice`),
  ADD KEY `INDEX_2` (`indice`),
  ADD KEY `indice` (`indice`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `aeropuertovuelos`
--
ALTER TABLE `aeropuertovuelos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;
--
-- AUTO_INCREMENT de la tabla `avionvuelos`
--
ALTER TABLE `avionvuelos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;
--
-- AUTO_INCREMENT de la tabla `usuariobilletes`
--
ALTER TABLE `usuariobilletes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `vuelos`
--
ALTER TABLE `vuelos`
  MODIFY `indice` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
